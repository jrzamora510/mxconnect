<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/global/plugins/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/register.js"><!-- comment --></script>
<script>jQuery.noConflict();</script>

<div class="container">
	
	<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

	<div class="portlet box green">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-gift"></i>Usuarios</div>
			
		</div>
		<div class="portlet-body">
		<c:if test="${errorOperacion!='0'}">
			<c:if test="${not empty resultadoOperacion}">
			<div class="alert alert-danger"  id="divMessage">
						<button class="close" data-close="alert"></button>
						<span id="spanMessage">${resultadoOperacion} </span>
			</div>
		</c:if>
		</c:if>
		<c:if test="${not empty resultadoOperacion && errorOperacion=='0'}">
			<div class="alert alert-success"  id="divMessage">
						<button class="close" data-close="alert"></button>
						<span id="spanMessage">${resultadoOperacion} </span>
			</div>
		</c:if>
			<!-- BEGIN FORM-->
			<form id="formUsuarios" class="form-horizontal">
			
				<div class="portlet-body" id="div_usuarios">
					<table class="table table-striped table-bordered table-hover" id="usuariosTable">
						<thead>
							<tr>
								<th>Usuario</th>
								<th>Usuario bloqueado</th>
								<th>Tipo de Usuario</th>
								<th></th>
							</tr>
						</thead>
						<tbody id="tabla_resumen_usuarios">
							<c:if test="${not empty listUsuarios}">
								<c:forEach items="${listUsuarios}" var="list">
									<tr>
										<td><c:out value="${list.user}"/></td>
										<td>
											<c:if test="${list.tipoUsuario == 'ROLE_USER'}">Usuario</c:if>
											<c:if test="${list.tipoUsuario == 'ROLE_ADMIN'}">Administrador</c:if>
										</td>
										<td>
											<c:if test="${list.bloqueado == 1}">No</c:if>
											<c:if test="${list.bloqueado != 1}">Si</c:if>
										</td>
										<td align="center"><input type="radio" name="opcion" id="opcion" value="${list.user}"> </td>
									</tr>
								</c:forEach>
							</c:if> 
						</tbody>
					</table>
				</div>
				<div class="form-actions">
						<div class="row">
							<div class="col-md-offset-3 col-md-9">
								<button type="button" class="btn btn-circle blue" id="eliminar" name="eliminar">Eliminar</button>
								&nbsp;&nbsp;
								<button type="button" class="btn btn-circle blue" id="bloquear" name="bloquear">Bloquear</button>
								&nbsp;&nbsp;
								<button type="button" class="btn btn-circle blue" id="desbloquear" name="desbloquear">Desbloquear</button>
								&nbsp;&nbsp;
								<a href="register.do">
									<button type="button" class="btn btn-circle blue">Alta</button>
								</a>&nbsp;&nbsp;
								<a href="${pageContext.request.contextPath}/dashboard.do">
									<button type="button" class="btn btn-circle blue">Cancelar</button>
								</a>
							</div>
						</div>
					</div>
			</form>
			<!-- END FORM-->
		</div>
	</div>

</div>
<!-- END PAGE CONTENT INNER -->
