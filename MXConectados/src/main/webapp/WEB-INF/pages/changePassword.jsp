<script type="text/javascript" src="assets/global/plugins/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/js/changePassword.js"><!-- comment --></script>

<div class="container">
	<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
	<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true"></button>
					<h4 class="modal-title">Gr�fico</h4>
				</div>
				<div class="modal-body">Ajustes</div>
				<div class="modal-footer">
					<button type="button" class="btn blue">Guardar cambios</button>
					<button type="button" class="btn default" data-dismiss="modal">Cerrar</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

	<div class="portlet box green">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-gift"></i>Cambiar Contrase�a</div>
			<div class="tools">
				<a href="javascript:;" class="collapse" data-original-title=""
					title=""> </a> <a href="#portlet-config" data-toggle="modal"
					class="config" data-original-title="" title=""> </a> <a
					href="javascript:;" class="reload" data-original-title="" title="">
				</a> <a href="javascript:;" class="remove" data-original-title=""
					title=""> </a>
			</div>
		</div>
		<div class="portlet-body form">
		<div class="alert alert-danger" style="display:none" id="divMessage">
					<button class="close" data-close="alert"></button>
					<span id="spanMessage">Contrase�a incorrecta, verifique. </span>
		</div>
			<!-- BEGIN FORM-->
			<form id="formPass" action="javascript:;" class="form-horizontal">
				<div class="form-body">
					<input type="hidden" id="user" value="${pageContext.request.userPrincipal.name}">
					<div class="form-group">
						<label class="col-md-3 control-label">Contrase�a</label>
						<div class="col-md-4">
								<input type="password" class="form-control input-circle" placeholder="" id="pass">  
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Nueva Contrase�a</label>
						<div class="col-md-4">
								<input type="password" class="form-control input-circle" placeholder="" id="newPass"> 
						</div>
						
					</div>
					<div class="form-group">
					<label class="col-md-3 control-label">Verificar nueva Contrase�a</label>
						<div class="col-md-4">
								<input type="password" class="form-control input-circle" placeholder="" id="verNewPass"> 
						</div>
					</div>
					<div class="form-actions">
						<div class="row">
							<div class="col-md-offset-3 col-md-9">
								<button type="submit" class="btn btn-circle blue">Cambiar</button>
								<a href="${pageContext.request.contextPath}/dashboard.do">
									<button type="button" class="btn btn-circle blue">Cancelar</button>
								</a>
							</div>
						</div>
					</div>
				</div>
			</form>
			<!-- END FORM-->
		</div>
	</div>

</div>
<!-- END PAGE CONTENT INNER -->
