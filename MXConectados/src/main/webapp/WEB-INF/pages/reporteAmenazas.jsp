<script type="text/javascript" src="assets/global/plugins/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/js/amenazasScript.js"><!-- comment --></script>

<div class="container">
	<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
	<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Gr�fico</h4>
				</div>
				<div class="modal-body">
					Ajustes
				</div>
				<div class="modal-footer">
					<button type="button" class="btn blue">Guardar cambios</button>
					<button type="button" class="btn default" data-dismiss="modal">Cerrar</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
	
	<!-- BEGIN PAGE CONTENT INNER -->
	<div class="row margin-top-10">

		<!-- <div class="col-md-12 col-sm-12">
			<div class="navbar navbar-default" role="navigation">
				Brand and toggle get grouped for better mobile display
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
						<span class="sr-only">
							Toggle navigation </span>
							<span class="icon-bar">
							</span>
							<span class="icon-bar">
							</span>
							<span class="icon-bar">
							</span>
						</button>
						<a class="navbar-brand" href="javascript:;">
							Ayuda </a>
						</div>
						Collect the nav links, forms, and other content for toggling
						<div class="collapse navbar-collapse navbar-ex1-collapse">
							<ul class="nav navbar-nav">
								<li id="01" class="active" onclick=""><a href="javascript:;">Aguascalientes</a></li>
								<li id="08" class="active" onclick=""><a href="javascript:;">Chihuahua</a></li>
								<li id="09" class="active" onclick=""><a href="javascript:;">Distrito Federal</a></li>
								<li id="15" class="active" onclick=""><a href="javascript:;">Estado de M�xico</a></li>
								<li id="14" class="active" onclick=""><a href="javascript:;">Jalisco</a></li>
								<li id="19" class="active" onclick=""><a href="javascript:;">Nuevo Le�n</a></li>
								<li id="21" class="active" onclick=""><a href="javascript:;">Puebla</a></li>
							</ul>
						</div>
						/.navbar-collapse
					</div>	
				</div> -->

				<div class="col-md-12 col-sm-12">
					<!-- BEGIN Portlet PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i>Reporte Top Ten Amenazas.
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="fullscreen">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<p>
								<strong>Descripcion del contenido</strong><br/>
								Incluir descripcion del contenido....
							</p>

							<div class="row">

								<div class="col-md-6">
									<!-- BEGIN BORDERED TABLE PORTLET-->
									<div class="portlet light">
										<div class="portlet-title">
											<div class="caption">
												<i class="fa fa-cogs font-green-sharp"></i>
												<span class="caption-subject font-green-sharp bold uppercase">Amenazas</span>
											</div>
											<div class="tools">
												<a href="javascript:;" class="collapse">
												</a>
												<a href="#portlet-config" data-toggle="modal" class="config">
												</a>
												<a href="javascript:;" class="reload">
												</a>
												<a href="javascript:;" class="remove">
												</a>
											</div>
										</div>
										<div class="portlet-body">
											<div class="table-scrollable">
												<table class="table table-bordered table-hover">
													<thead>
														<tr>
															<th>
																Amenaza
															</th>
															<th>
																No. de eventos
															</th>
															<th>
																Pto
															</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>
																DeepThroat
															</td>
															<td>
																230
															</td>
															<td>
																41
															</td>
														</tr>
														<tr>
															<td>
																dmsetup
															</td>
															<td>
																310
															</td>
															<td>
																58
															</td>
														</tr>
														<tr>
															<td>
																FC infecto
															</td>
															<td>
																150
															</td>
															<td>
																146
															</td>
															
														</tr>
														<tr>
															
															<td>
																Sandy
															</td>
															<td>
																Lim
															</td>
															<td>
																sanlim
															</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
									<!-- END BORDERED TABLE PORTLET-->
								</div>

								<div class="col-md-6" >
									<div class="encabezadoTabla portlet light ">
										<div class="encabezadoTabla portlet-title ">
											<div class="caption">
												<i class="icon-bar-chart font-green-haze"></i>
												<span class="caption-subject bold uppercase font-green-haze ">Eventos por amenaza</span>
											</div>
											<div class="tools">
												<a href="javascript:;" class="collapse">
												</a>
												<a href="${pageContext.request.contextPath}/" data-toggle="modal" class="config">
												</a>
												<a href="javascript:;" class="reload">
												</a>
												<a href="javascript:;" class="fullscreen">
												</a>
												<a href="javascript:;" class="remove">
												</a>
											</div>
										</div>
										<div class="portlet-body">
											<div id="chart_1" class="chart" >
											</div>
										</div>
									</div>
								</div>


							</div>

						</div>
					</div>
					<!-- END Portlet PORTLET-->
				</div>
			</div>


		</div>
		<!-- END PAGE CONTENT INNER -->
</div> <!--  Div Container -->