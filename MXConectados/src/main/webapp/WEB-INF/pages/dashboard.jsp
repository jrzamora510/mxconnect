<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/global/plugins/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/js/dashboardScript.js"><!-- comment --></script>
<script src="${pageContext.request.contextPath}/js/gidCharts.js"><!-- comment --></script>
<script>jQuery.noConflict();</script>

<div class="container">
	<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
	<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true"></button>
					<h4 class="modal-title">Gr�fico</h4>
				</div>
				<div class="modal-body">Ajustes</div>
				<div class="modal-footer">
					<button type="button" class="btn blue">Guardar cambios</button>
					<button type="button" class="btn default" data-dismiss="modal">Cerrar</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
	<!-- BEGIN PAGE BREADCRUMB -->
	<%-- <ul class="page-breadcrumb breadcrumb">
		<li><a href="${pageContext.request.contextPath}/">Inicio</a><i
			class="fa fa-circle"></i></li>
		<li class="active">Dashboard</li>
	</ul> --%>
	<!-- END PAGE BREADCRUMB -->


	<!-- BEGIN PAGE CONTENT INNER -->
	<div class="row margin-top-10">

		<div class="col-md-12 col-sm-12">
			<div class="navbar navbar-default" role="navigation">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse"
						data-target=".navbar-ex1-collapse">
						<span class="sr-only"> Toggle navigation </span> <span
							class="icon-bar"> </span> <span class="icon-bar"> </span> <span
							class="icon-bar"> </span>
					</button>
				<!--	<a class="navbar-brand" href="javascript:;"> Ayuda </a> -->
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse navbar-ex1-collapse">
					<ul class="nav navbar-nav" id="statesButtons">
						<li id="1" onclick=""><a href="#">Aguascalientes</a></li>
						<li id="6" onclick=""><a href="#">Chihuahua</a></li>
						<li id="9" onclick=""><a href="#">Distrito Federal</a></li>
						<li id="15" onclick=""><a href="#">Estado de M&eacute;xico</a></li>
						<li id="14" onclick=""><a href="#">Jalisco</a></li>
						<li id="19" onclick=""><a href="#">Nuevo Le&oacute;n</a></li>
						<li id="24" onclick=""><a href="#">San Luis Potos&iacute;</a></li>
						<li id="21" onclick=""><a href="#">Puebla</a></li>
						<li id="22" onclick=""><a href="#">Quer&eacute;taro</a></li>
					</ul>
					
				</div>
				<!-- /.navbar-collapse -->
			</div>
		</div>

		<div class="col-md-12 col-sm-12">
			<!-- BEGIN EXAMPLE TABLE PORTLET-->
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-cogs font-green-sharp"></i><span class="caption-subject font-green-sharp bold uppercase" id="stateText">Resumen Jalisco</span>
					</div>
					<form id="formGid" class="navbar-form navbar-right" role="search">
						<div class="form-group">
							<input name="searchByGID" id="searchByGID" class="form-control"
								placeholder="Filtrar por GID" type="text">
						</div>
						<button id="searchByGIDButton" type="button" class="btn blue">Buscar</button>
					</form>
					<div class="tools"></div>
				</div>
				<div class="portlet-body" id="blockui_sample_1_portlet_body">
					<table class="table table-striped table-bordered table-hover" id="dashboardResumeTable">
						<thead>
							<tr>
								<th>GID</th>
								<th>Fecha de instalaci&oacute;n Axtel</th>
								<th>Fecha de Firma de Acta <br/>e Inicio de Operaci&oacute;n</th>
								<th>Ancho de banda <br/>subida(mb/s)</th>
								<th>Ancho de banda <br/>bajada(mb/s)</th>
								<th>IP homologada</th>
								<th>Estado del servicio</th>
								<th>Tr&aacute;fico</th>
								<th>Estado</th>
								<th>Municipio</th>
								<th>Localidad</th>
								<th>Dependencia</th>
								<th>Ancho de banda <br/>subida adjudicado(mb/s)</th>
								<th>Ancho de banda <br/>bajada adjudicado(mb/s)</th>
								<th>Latencia<br/>(milisegundos)</th>
							</tr>
						</thead>
						<tbody id="tabla_resumen_body">
							<c:if test="${not empty tableRows}">
								<c:forEach items="${tableRows}" var="row">
									<tr onclick="setupDashboardResumeTableClicks()">
										<td>${row.gID}</td>
										<td>${row.fechaInstSitioAxtel}</td>
										<td>${row.fechaFirmaActaInicioOper}</td>
										<td>${row.anchoBandaSubida}</td>
										<td>${row.anchoBandaBajada}</td>
										<td>${row.ipHomologada}</td>
										<td>${row.estadoDelServicio}</td>
										<td>${row.trafico}</td>
										<td>${row.entidadFederativa}</td>
										<td>${row.municipio}</td>
										<td>${row.localidad}</td>
										<td>${row.institucion}</td>
										<td>${row.tasaTransferenciaSubida}</td>
										<td>${row.tasaTransferenciaBajada}</td>
										<td>${row.latencia}</td>
									</tr>
								</c:forEach>
							</c:if>
						</tbody>
					</table>
				</div>
			</div>
			<!-- END EXAMPLE TABLE PORTLET-->
		</div>
		<div class="col-md-12" id="moreUsedProtocolsTableDiv" style="display: none;">
			<!-- BEGIN BORDERED TABLE PORTLET-->
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-cogs font-green-sharp"></i> <span
							class="caption-subject font-green-sharp bold uppercase">Protocolos mas utilizados</span>
					</div>
					<!-- <div class="tools">
						<a href="javascript:;" class="collapse"> </a> <a
							href="#portlet-config" data-toggle="modal" class="config"> </a> <a
							href="javascript:;" class="reload"> </a> <a href="javascript:;"
							class="remove"> </a>
					</div> -->
				</div>
				<div class="portlet-body display-hide" id="moreUsedProtocolsTablePortletBodyDiv">
					<div class="table-scrollable">
						<table class="table table-bordered table-hover" id="moreUsedProtocolsTable" >
							<thead>
								<tr>
									<th>Protocolo</th>
									<th>Visitas</th>
									<th>%</th>
								</tr>
							</thead>
							<tbody id="tabla_protocolos_body">
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- END BORDERED TABLE PORTLET-->
		</div>

		<div class="col-md-6" id="moreUsedProtocolsChartDiv" style="display: none;">
			<div class="encabezadoTabla portlet light ">
				<div class="encabezadoTabla portlet-title ">
					<div class="caption">
						<i class="icon-bar-chart font-green-haze"></i> <span class="caption-subject bold uppercase font-green-haze ">Protocolos	mas utilizados</span>
					</div>
					<%-- <div class="tools">
						<a href="javascript:;" class="collapse"> </a> <a
							href="${pageContext.request.contextPath}/" data-toggle="modal"
							class="config"> </a> <a href="javascript:;" class="reload"> </a>
						<a href="javascript:;" class="fullscreen"> </a> <a
							href="javascript:;" class="remove"> </a>
					</div> --%>
				</div>
				<div class="portlet-body display-hide" id="moreUsedProtocolsChartPortletBodyDiv">
					<div id="moreUsedProtocolsChart" class="chart"></div>
				</div>
			</div>
		</div>

		<div class="col-md-6" id="bandWidthDiv" style="display: none;">
			<div class="encabezadoTabla portlet light ">
				<div class="encabezadoTabla portlet-title ">
					<div class="caption">
						<i class="icon-bar-chart font-green-haze"></i> <span class="caption-subject bold uppercase font-green-haze ">Ancho de banda</span>
					</div>
					<%-- <div class="tools">
						<a href="javascript:;" class="collapse"> </a> <a
							href="${pageContext.request.contextPath}/" data-toggle="modal"
							class="config"> </a> <a href="javascript:;" class="reload"> </a>
						<a href="javascript:;" class="fullscreen"> </a> <a
							href="javascript:;" class="remove"> </a>
					</div> --%>
				</div>
				<div class="portlet-body display-hide" id="bandWidthPortletBodyDiv">
					<div id="bandWidthChart" class="chart"></div>
				</div>
			</div>
		</div>

		<div class="col-md-6" id="transferRateChartDiv" style="display: none;">
			<div class="encabezadoTabla portlet light ">
				<div class="encabezadoTabla portlet-title ">
					<div class="caption">
						<i class="icon-bar-chart font-green-haze"></i> <span class="caption-subject bold uppercase font-green-haze ">Tasa de transferencia</span>
					</div>
					<%-- <div class="tools">
						<a href="javascript:;" class="collapse"> </a> <a
							href="${pageContext.request.contextPath}/" data-toggle="modal"
							class="config"> </a> <a href="javascript:;" class="reload"> </a>
						<a href="javascript:;" class="fullscreen"> </a> <a
							href="javascript:;" class="remove"> </a>
					</div> --%>
				</div>
				<div class="portlet-body display-hide" id="transferRateChartPortletBodyDiv">
					<div id="transferRateChart" class="chart"></div>
				</div>
			</div>
		</div>

		<div class="col-md-6" id="latenciChartDiv" style="display: none;">
			<div class="encabezadoTabla portlet light ">
				<div class="encabezadoTabla portlet-title ">
					<div class="caption">
						<i class="icon-bar-chart font-green-haze"></i> <span class="caption-subject bold uppercase font-green-haze ">Latencia</span>
					</div>
					<%-- <div class="tools">
						<a href="javascript:;" class="collapse"> </a> <a
							href="${pageContext.request.contextPath}/" data-toggle="modal"
							class="config"> </a> <a href="javascript:;" class="reload"> </a>
						<a href="javascript:;" class="fullscreen"> </a> <a
							href="javascript:;" class="remove"> </a>
					</div> --%>
				</div>
				<div class="portlet-body display-hide" id="latenciChartPortletBodyDiv">
					<div id="latenciChart" class="chart"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- END PAGE CONTENT INNER -->
</div>
<!--  Div Container -->