<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<script type="text/javascript" src="assets/global/plugins/jquery.min.js"></script>
<script type="text/javascript" >
	$( "#liDashboard" ).removeClass("active");
	$( "#liHelp" ).addClass("active");
state.addClass("active");
</script>


<div class="container">
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="portlet light">
				<div class="portlet-body">
					<div class="row">
						<div class="col-md-3">
							<ul class="ver-inline-menu tabbable margin-bottom-10">
								<li class="active">
									<a data-toggle="tab" href="#tab_1">
									<i class="fa fa-briefcase"></i> Portal</a>
									<span class="after">
									</span>
								</li>
								<!-- <li>
									<a data-toggle="tab" href="#tab_2">
									<i class="fa fa-table"></i> Consultas </a>
								</li>
								<li>
									<a data-toggle="tab" href="#tab_3">
									<i class="fa fa-bar-chart-o"></i> Graficas </a>
								</li>
								<li>
									<a data-toggle="tab" href="#tab_5">
									<i class="fa fa-group"></i> Usuarios </a>
								</li> -->
							</ul>
						</div>
						<div class="col-md-9">
							<div class="tab-content">
							
								<div id="tab_1" class="tab-pane active">
									<div id="accordion1" class="panel-group">
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
												<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_2">
												<b>Informaci�n mostrada en la secci�n Resumen</b>
												</a>
												</h4>
											</div>
											<div id="accordion1_2" class="panel-collapse collapse">
												<div class="panel-body">
													 <img alt="" src="${pageContext.request.contextPath}/img/resumen.jpg">
													 <br>
													 En la secci�n Resumen del Portal, se muestran los siguientes elementos:
													 <br>
													 - Muestra registros. En esta opci�n se selecciona cuantos registros se mostrar�n en la lista.
													 <br>
													 - Buscar. B�squeda por un GID en espec�fico.
													 <br>
													 - Excel. Se importa a un archivo Excel la lista de registros encontrados.
													 <br>
													 - Imprimir. Permite mandar a impresi�n la pantalla.
													 <br>
													 - Lista.
													 <br>
													 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- GID. 
													 <br>
													 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Fecha de Instalaci�n Axtel.
													 <br>
													 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Fecha de Firma de Acta e Inicio de Operaci�n.
													 <br>
													 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Ancho de Banda Subida (Bit/s).
													 <br>
													 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Ancho de Banda Bajada (Bit/s).
													 <br>
													 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- IP Homologada.
													 <br>
													 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Estado del Servicio. 
													 <br>
													 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Tr�fico.
													 <br>
													 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Estado. Estado de la Rep�blica en la que se encuentra f�sicamente el servidor.
													 <br>
													 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Municipio. Municipio en el que se encuentra f�sicamente el servidor.
													 <br>
													 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Localidad. Localidad en la que se encuentra f�sicamente el servidor.
													 <br>
													 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Instituci�n.  Instituci�n en la que se encuentra f�sicamente el servidor.
													 <br>
													 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Tasa de Transferencia Subida (Bit/s).
													 <br>
													 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Tasa de Transferencia Bajada (Bit/s).
													 <br>
													 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Latencia (Milisegundos).
													 <br>
													 - Cantidad de registros mostrados.
													 <br>
													 - Paginado
												</div>
											</div>
										</div>
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
												<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_3">
												<b>Funcionalidad para cada registro.</b>
												</a>
												</h4>
											</div>
											<div id="accordion1_3" class="panel-collapse collapse">
												<div class="panel-body">
													 
													 Al seleccionar uno de los registros, el sistema realiza una busqueda de informaci�n para mostrar el siguiente detalle:
													 <br>
													 - Lista de Protocolos m�s Utilizados. En esta lista se muestran los siguientes conceptos:
													 <br>
													 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Protocolo. Protocolos m�s utilizados iniciando por el m�s visitado.
													 <br>
													 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Visitas. N�mero de visitas que tuvo en las �ltimas 24 horas.
													 <br>
													 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- %. Porcentaje que ocupa en el universo de protocolos.
													 <br>
													 &nbsp;&nbsp;&nbsp;&nbsp;<img alt="" src="${pageContext.request.contextPath}/img/protocolos.jpg">
													 <br>
													 - Gr�fica de los Protocolos m�s Utilizados. Muestra los protocolos m�s utilizados y el n�mero de visitas que ha tenido en las �ltimas 24 horas.
													 <br>
													 &nbsp;&nbsp;&nbsp;&nbsp;<img alt="" src="${pageContext.request.contextPath}/img/grafProtocolos.jpg">
													 <br>
													 - Grafica de Ancho de Banda (Subida). Muestra el ancho de banda en megabits/s que tuvo el servidor en un periodo de tiempo.
													 <br>
													 &nbsp;&nbsp;&nbsp;&nbsp;<img alt="" src="${pageContext.request.contextPath}/img/grafAncho.jpg">
													 <br>
													 - Gr�fica de Tasa de Transferencia. Muestra la tasa de transferencia en bits/s que tuvo el servidor en un periodo de tiempo.
													 <br>
													 &nbsp;&nbsp;&nbsp;&nbsp;<img alt="" src="${pageContext.request.contextPath}/img/grafTransf.jpg">
													 <br>
													 - Gr�fica de Latencia. Muestra la latencia en milisegundos que tuvo el servidor en un periodo de tiempo.
													 <br>
													 &nbsp;&nbsp;&nbsp;&nbsp;<img alt="" src="${pageContext.request.contextPath}/img/grafLatencia.jpg">
													 <br>
												</div>
											</div>
										</div>
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
												<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_1">
												<b>Acerca de este proyecto.</b></a>
												</h4>
											</div>
											<div id="accordion1_1" class="panel-collapse collapse in">
												<div class="panel-body">
													 <div class="page-logo" align="center" >
														<h6 align="center">
														<small>S.C.T.  A X T E L<br>
														LICITACI�N P�BLICA NACIONAL ELECTR�NICA<br>
														<b>No LA-009000937-N10-2014<br>
														"SERVICIOS DE CONECTIVIDAD A INTERNET EN SITIOS P�BLICOS PARA LA RED M�XICO CONECTADO 2"</b>
														</small>
														</h6>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								
								<div id="tab_2" class="tab-pane">
									<div id="accordion2" class="panel-group">
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
												<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#accordion2_1">
												Dashboard
												</a>
												</h4>
											</div>
											<div id="accordion2_1" class="panel-collapse collapse in">
												<div class="panel-body">
													<p>
														Al momento de iniciar sesion el sistema lo lleva directamente a la pagina de dashboard donde se mostrara una tabla con los datos del estado por defecto que es jalisco.
														En la parte superior se encuentra un menu con los diferentes estados, al seleccionar alguno la tabla se actualizara para mostrar los datos del estado seleccionado.
														Ademas, en la parte derecha de la tabla puede hacer una consulta de un GID en particular, tenga en cuenta que al hacer la busqueda por GID este buscara en todos los estados y no unicamente en el seleccionado. 
													</p>
												</div>
											</div>
										</div>
										
									</div>
								</div>
								
								<div id="tab_3" class="tab-pane">
									<div id="accordion3" class="panel-group">
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
												<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#accordion3_6">
													Ver informacion de un GID en particular.
												</a>
												</h4>
											</div>
											<div id="accordion3_6" class="panel-collapse collapse in">
												<div class="panel-body">
													 Cuando se encuantra consultado la informacion de un estado es posble conocer informacion sobre el ancho de banda, latencia tasa de transferencia y protocolos mas utilizados. Para ver esta informacion 
													basta con hacer clic sobre alguna fila y el sistema mostrara graficas de la columna seleccionada.
												</div>
											</div>
										</div>
									</div>
								</div>
								
								<div id="tab_4" class="tab-pane">
									<div id="accordion3" class="panel-group">
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
												<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#accordion3_6">
													TODO
												</a>
												</h4>
											</div>
											<div id="accordion3_6" class="panel-collapse collapse in">
												<div class="panel-body">
													 TODO
												</div>
											</div>
										</div>
									</div>
								</div>
								
								<div id="tab_5" class="tab-pane">
									<div id="accordion3" class="panel-group">
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
												<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#accordion3_6">
													TODO
												</a>
												</h4>
											</div>
											<div id="accordion3_6" class="panel-collapse collapse in">
												<div class="panel-body">
													 TODO
												</div>
											</div>
										</div>
									</div>
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
<!--  Div Container -->