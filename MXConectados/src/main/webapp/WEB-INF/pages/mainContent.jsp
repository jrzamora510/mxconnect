		<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
		<div class="container">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Gr�fico</h4>
						</div>
						<div class="modal-body">
							 Ajustes
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Guardar cambios</button>
							<button type="button" class="btn default" data-dismiss="modal">Cerrar</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE BREADCRUMB -->
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="${pageContext.request.contextPath}/">Inicio</a><i class="fa fa-circle"></i>
				</li>
				<li class="active">
					 Dashboard
				</li>
			</ul>
			<!-- END PAGE BREADCRUMB -->
			
			
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
			
				<div class="col-md-12 col-sm-12">
					<div class="navbar navbar-default" role="navigation">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
						<span class="sr-only">
						Toggle navigation </span>
						<span class="icon-bar">
						</span>
						<span class="icon-bar">
						</span>
						<span class="icon-bar">
						</span>
						</button>
						<a class="navbar-brand" href="javascript:;">
						Ayuda </a>
					</div>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse navbar-ex1-collapse">
						<ul class="nav navbar-nav">
							<li id="01" class="active" onclick=""><a href="javascript:;">Aguascalientes</a></li>
							<li id="08" class="active" onclick=""><a href="javascript:;">Chihuahua</a></li>
							<li id="09" class="active" onclick=""><a href="javascript:;">Distrito Federal</a></li>
							<li id="15" class="active" onclick=""><a href="javascript:;">Estado de M�xico</a></li>
							<li id="14" class="active" onclick=""><a href="javascript:;">Jalisco</a></li>
							<li id="19" class="active" onclick=""><a href="javascript:;">Nuevo Le�n</a></li>
							<li id="21" class="active" onclick=""><a href="javascript:;">Puebla</a></li>
						</ul>
						<form class="navbar-form navbar-right" role="search">
							<div class="form-group">
								<input name="searchByGID" class="form-control" placeholder="Filtrar por GID" type="text">
							</div>
							<button type="submit" class="btn blue">Buscar</button>
						</form>
					</div>
					<!-- /.navbar-collapse -->
					</div>	
				
				</div>
				
				<div class="col-md-12 col-sm-12">
					<!-- BEGIN SAMPLE TABLE PORTLET-->
					<div class="portlet light">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-cogs font-green-sharp"></i>
								<span class="caption-subject font-green-sharp bold uppercase">Resumen <c:out value="${requestScope.QueryState}"></c:out> </span>
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="${pageContext.request.contextPath}/" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-scrollable">
								<table class="table table-striped table-bordered table-hover">
								<thead>
								<tr>
									<th scope="col" style="width:450px !important">
										 GID
									</th>
									<th scope="col">
										 Sitios Instalados - Axtel
									</th>
									<th scope="col">
										 Fecha Inicio de Operaci�n
									</th>
									<th scope="col">
										 Ancho de banda - Subida
									</th>
									<th scope="col">
										 Ancho de banda - Bajada
									</th>
									<th scope="col">
										 IP homologada										 
									</th>
									<th scope="col">
										 Estado del servicio
									</th>
									<th scope="col">
										 Tr�fico
									</th>
									<th scope="col">
										 Estado
									</th>
									<th scope="col">
										 Municipio 
									</th>
									<th scope="col">
										 Localidad 
									</th>
									<th scope="col">
										 Instituci�n 
									</th>
									<th scope="col">
										 Tasa de transferencia - Subida 
									</th>
									<th scope="col">
										 Tasa de transferencia - Bajada 
									</th>
									<th scope="col">
										 Latencia
									</th>
								</tr>
								</thead>
								<tbody>
								
								<c:forEach var="data" items="${MainTable}" >
								<tr>
									<td>
										 ${data["gID"]}
									</td>
									<td>
										 ${data["fechaInstSitioAxtel"]}
									</td>
									<td>
										 ${data["fechaFirmaActaInicioOper"]}
									</td>
									<td>
										 ${data["anchoBandaSubida"]}
									</td>
									<td>
										 ${data["anchoBandaBajada"]}
									</td>
									<td>
										 ${data["ipHomologada"]}
									</td>
									<td>
										${data["estadoDelServicio"]}
									</td>
									<td>
										 ${data["trafico"]}
									</td>
									<td>
										 ${data["entidadFederativa"]}
									</td>
									<td>
										${data["municipio"]}
									</td>
								
									<td>
										 ${data["localidad"]}
									</td>
									<td>
										 ${data["institucion"]}
									</td>
									<td>
										 ${data["tasaTransferenciaSubida"]}
									</td>
									<td>
										 ${data["tasaTransferenciaBajada"]}
									</td>
									<td>
										 ${data["latencia"]}
									</td>
								</tr>
								</c:forEach>
								
								</tbody>
								</table>
							</div>
						</div>
					</div>
					<!-- END SAMPLE TABLE PORTLET-->
				</div>
			
				<div class="col-md-6 col-sm-12">
					<!-- BEGIN CHART PORTLET-->
					<!--  Graph 1 -->
					<!-- END CHART PORTLET-->
				</div>
				<div class="col-md-6 col-sm-12">
					<!-- BEGIN CHART PORTLET-->
					<!-- Graph 2 -->
					<!-- END CHART PORTLET-->
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<!-- BEGIN CHART PORTLET-->
					<!-- Graph 3 -->
					<!-- END CHART PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div> <!--  Div Container -->