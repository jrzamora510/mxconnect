<div class="container">
	<div class="container">
		<div class="row">
			<div class="col-md-12 page-404">
				<div class="number">ERROR</div>
				<div class="details">
					<h3>Lo sentimos.</h3>
					<p>
						Por el momento, el recurso que desea acceder no est&aacute; disponible.<br>
						<a href="${pageContext.request.contextPath}/dashboard.do"> Regrese a la p&aacute;gina principal</a> o seleccione una opci&oacute;n del men&uacute;.
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
