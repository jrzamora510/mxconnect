
		<div class="container">
			<!-- BEGIN LOGO -->
			<div class="page-logo" >
				<a href=""><img src="${pageContext.request.contextPath}/img/mxConectado.png" alt="M�xico Conectado" style=" height: 98%"></a>
			</div>
			<!-- END LOGO -->
			
			<div class="page-logo" align="center" >
				<h6 align="center">
				<small>S.C.T.  A X T E L<br>
				LICITACI�N P�BLICA NACIONAL ELECTR�NICA<br>
				<b>No LA-009000937-N10-2014<br>
				"SERVICIOS DE CONECTIVIDAD A INTERNET EN SITIOS P�BLICOS PARA LA RED M�XICO CONECTADO 2"</b>
				</small>
				</h6>
			</div>
						
			<div class="page-logo" align="right" >
				<a href=""><img src="${pageContext.request.contextPath}/img/logoSCT_hoz.png" alt="SCT" style=" height: 98%"></a>
			</div>
			
			
			<!-- BEGIN RESPONSIVE MENU TOGGLER -->
			<a href="javascript:;" class="menu-toggler"></a>
			<!-- END RESPONSIVE MENU TOGGLER -->
			<!-- BEGIN TOP NAVIGATION MENU -->
			<div class="top-menu">
				<ul class="nav navbar-nav pull-right">
					<!-- BEGIN NOTIFICATION DROPDOWN -->
					<!-- END NOTIFICATION DROPDOWN -->
					<!-- BEGIN TODO DROPDOWN -->
					
					<!-- END TODO DROPDOWN -->
					<li class="droddown dropdown-separator">
						<span class="separator"></span>
					</li>
					<!-- BEGIN INBOX DROPDOWN -->
					
					<!-- END INBOX DROPDOWN -->
					<!-- BEGIN USER LOGIN DROPDOWN -->
					<li class="dropdown dropdown-user dropdown-dark">
						<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						<img alt="" class="img-circle" src="${pageContext.request.contextPath}/assets/admin/layout3/img/avatar.png">
						<span class="username username-hide-mobile">
							${pageContext.request.userPrincipal.name}
						</span>
						</a>
						<ul class="dropdown-menu dropdown-menu-default">
							<li>
								<a href="${pageContext.request.contextPath}/changePassword.do">
								<i class="icon-user"></i> Cambiar contrase�a </a>
							</li>
							<li class="divider">
							</li>
							<%-- <li>
								<a href="${pageContext.request.contextPath}/lock.jsp">
								<i class="icon-lock"></i> Bloquear pantalla </a>
							</li> --%>
							<li>
								<a href="${pageContext.request.contextPath}/logout">
								<i class="icon-key"></i> Salir </a>
							</li>
						</ul>
					</li>
					<!-- END USER LOGIN DROPDOWN -->
				</ul>
			</div>
			<!-- END TOP NAVIGATION MENU -->
		</div>
		