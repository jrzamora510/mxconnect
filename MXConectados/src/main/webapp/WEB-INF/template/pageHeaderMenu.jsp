<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

		<div class="container">
			<!-- BEGIN HEADER SEARCH BOX -->
			
			<!-- END HEADER SEARCH BOX -->
			<!-- BEGIN MEGA MENU -->
			<!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
			<!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
			<div class="hor-menu ">
				<ul class="nav navbar-nav">
				<li class="" id="liHelp">
						<a href="${pageContext.request.contextPath}/help.do">Ayuda</a>
					</li>
					<li class="active" id="liDashboard">
						<a href="${pageContext.request.contextPath}/dashboard.do">Portal</a>
					</li>
					<li class="" id="lieSight">
						<a href="https://189.209.97.82:31943">eSight</a>
					</li>
					<li class="" id="liLogCenter">
						<a href="https://189.209.97.83:443">LogCenter</a>
					</li>
					
					<%-- <li class="menu-dropdown mega-menu-dropdown " id="liReports">
						<a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;" class="dropdown-toggle"> Reportes <i class="fa fa-angle-down"></i>
						</a>
						<ul class="dropdown-menu" style="min-width: 510px">
							<li>
								<div class="mega-menu-content">
									<div class="row">
										<div class="col-md-4">
											<ul class="mega-menu-submenu">
												<li>
													<h3>Top Ten</h3>
												</li>
												<li>
													<a href="${pageContext.request.contextPath}/reporteAmenazas.do" class="iconify">
													<i class="icon-paper-plane"></i>
													Amenazas </a>
												</li>
												<li>
													<a href="" class="iconify">
													<i class="icon-paper-plane"></i>
													IPs o grupo de Categoria de Red Bloqueados </a>
												</li>
											</ul>
										</div>
										<div class="col-md-4">
											<ul class="mega-menu-submenu">
												<li>
													<h3>Miscelaneos</h3>
												</li>
												<li>
													<a href="" class="iconify">
													<i class="icon-bar-chart"></i>
													Categor�a de red con Mayor Tr�fico </a>
												</li>
												<li>
													<a href="" class="iconify">
													<i class="icon-bar-chart"></i>
													Sitios M�s Visitados </a>
												</li>
												<li>
													<a href="" class="iconify">
													<i class="icon-bar-chart"></i>
													Consumo de Ancho de Banda </a>
												</li>
												<li>
													<a href="" class="iconify">
													<i class="icon-bar-chart"></i>
													Intentos por Sitio </a>
												</li>
												<li>
													<a href="" class="iconify">
													<i class="icon-bar-chart"></i>
													Politicas Aplicadas a Filtrado de Contenidos </a>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</li>
						</ul>
					</li> --%>
					<sec:authorize access="hasRole('ADMIN')">
 						<li id="liRegister">
							<a href="${pageContext.request.contextPath}/usuarios.do">Usuarios</a>
						</li> 
						<!-- <li id="liRegister">
							<a href="${pageContext.request.contextPath}/register.do">Nuevo usuario</a>
						</li> -->
					</sec:authorize>
				</ul>
			</div>
			<!-- END MEGA MENU -->
		</div>
	