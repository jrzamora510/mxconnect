$("#liDashboard").removeClass( "active" );
$("#liReports").removeClass( "active" );
$("#liRegister").removeClass( "active" );

$(document).ready(function() {
	
	$("#formPass").submit(function (e) {
		console.log('submit');
        e.preventDefault();
        var user = $("#user").val();
        var pass = $("#pass").val();
        var newPass = $("#newPass").val();
        var verNewPass = $("#verNewPass").val();
		
        if(user.length == 0){
        	$("#divMessage").show();
			$("#spanMessage").text('Debe ingresar un usuario');
        }else if( newPass.length < 8 ){
			$("#divMessage").show();
			$("#spanMessage").text('La nueva contraseña debe ser de al menos 8 caracteres');
		}else if( newPass != verNewPass ){
			$("#divMessage").show();
			$("#spanMessage").text('Verifique que su Contraseña conicida');
		}else{
			var data = {
	    			user: user,
	    			password: pass,
	    			nuevoPassword: newPass,
	    	};
	        
	        console.log(data);
	        
	        var urlPost = "cambioPassword.do";

	    	$.ajax({
	    		type: 'POST',
	    		url: urlPost,
	    		dataType: "json",
	    		data: JSON.stringify(data), 
	    		contentType: 'application/json',
	    		mimeType: 'application/json',
	    		beforeSend: function(xhr) {
	    			xhr.overrideMimeType('application/json; charset=iso-8859-1');
	    		},
	    		success: function(resp) {
	    			console.log(resp);
	    			if(resp.codigo == "OK"){
	    				$("#divMessage").show();
	    				$("#spanMessage").text('Se cambio la contraseña con exito');
	    				$("#divMessage").removeClass( "alert-danger" ).addClass( "alert-success" );
	    			}else{
	    				$("#divMessage").show();
	    				$("#spanMessage").text('Error: ' + resp.codigo + '. ' + resp.mensaje);
	    				$("#divMessage").removeClass( "alert-success" ).addClass( "alert-danger" );
	    			}
	    		},
	    		error: function (err) {
	    			console.log(err);
	    		},complete: function(){
	    			Metronic.stopPageLoading();
	    		}
	    	});
	    }
	});
});