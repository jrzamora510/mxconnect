	
	function initMoreUsedProtocolsChart(chartData) {
        var chart = AmCharts.makeChart("moreUsedProtocolsChart", {
            "theme": "light",
            "type": "serial",
            "dataProvider": chartData,
            "valueAxes": [{
                "position": "left",
                "title": "visitas"
            }],
            "graphs": [{
                "balloonText": "Protocolo [[category]]:[[value]]",
                "fillColorsField": "color",
                "fillAlphas": 0.9,
                "lineAlpha": 0.2,
                "type": "column",
                "valueField": "total"
              }],
              "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": false
              },
              "depth3D": 20,
              "angle": 30,
              "categoryField": "protocol",
              "categoryAxis": {
                "gridPosition": "start"
            },
            "exportConfig":{
                "menuTop":"20px",
                "menuRight":"20px",
                "menuItems": [{
                "icon": 'http://www.amcharts.com/lib/3/images/export.png',
                "format": 'png'   
                }]  
            }
        });

        $('#moreUsedProtocolsChart').closest('.portlet').find('.fullscreen').click(function() {
            chart.invalidateSize();
        });
    }

    function initBandWidthChart(chartData) {
        var chart = AmCharts.makeChart("bandWidthChart", {
            "type": "serial",
            "theme": "none",
            "pathToImages": "http://www.amcharts.com/lib/3/images/",
            "dataProvider": chartData,
            "valueAxes": [{
                "position": "left",
                "title": "megabits"
            }],
            "graphs": [{
                "fillAlphas": 0.4,
                "valueField": "downBytes"
            }],
            "chartScrollbar": {},
            "chartCursor": {
                "categoryBalloonDateFormat": "JJ:NN, DD MMMM",
                "cursorPosition": "mouse"
            },
            "categoryField": "time",
            "categoryAxis": {
                "minPeriod": "mm",
                "parseDates": true
            }
        });

        $('#bandWidthChart').closest('.portlet').find('.fullscreen').click(function() {
            chart.invalidateSize();
        });
    }

    function initTransferRateChart(chartData) {
        var chart = AmCharts.makeChart("transferRateChart", {
            "type": "serial",
            "theme": "none",
            "pathToImages": "http://www.amcharts.com/lib/3/images/",
            "dataProvider": chartData,
            "valueAxes": [{
                "position": "left",
                "title": "bits"
            }],
            "graphs": [{
                "fillAlphas": 0.4,
                "valueField": "transferRateIn"
            }],
            "chartScrollbar": {},
            "chartCursor": {
                "categoryBalloonDateFormat": "JJ:NN, DD MMMM",
                "cursorPosition": "mouse"
            },
            "categoryField": "time",
            "categoryAxis": {
                "minPeriod": "mm",
                "parseDates": true
            }
        });

        $('#transferRateChart').closest('.portlet').find('.fullscreen').click(function() {
            chart.invalidateSize();
        });
    }


    function initLatencyChart(chartData) {
        var chart = AmCharts.makeChart("latenciChart", {
            "type": "serial",
            "theme": "none",
            "pathToImages": "http://www.amcharts.com/lib/3/images/",
            "dataProvider": chartData,
            "valueAxes": [{
                "position": "left",
                "title": "milisegundos"
            }],
            "graphs": [{
                "fillAlphas": 0.4,
                "valueField": "latency"
            }],
            "chartScrollbar": {},
            "chartCursor": {
                "categoryBalloonDateFormat": "JJ:NN, DD MMMM",
                "cursorPosition": "mouse"
            },
            "categoryField": "time",
            "categoryAxis": {
                "minPeriod": "mm",
                "parseDates": true
            }
        });

        $('#latenciChart').closest('.portlet').find('.fullscreen').click(function() {
            chart.invalidateSize();
        });
    }
