$("#liDashboard").removeClass( "active" );
$("#liReports").removeClass( "active" );
$("#liRegister").addClass( "active" );

$(document).ready(function() {
	
	$("#formRegister").submit(function (e) {
        e.preventDefault();
        var user = $("#user").val();
        var pass = $("#pass").val();
        var verPass = $("#verPass").val();
        var userType = $("#userTypeSelect").val();
		
        if(user.length == 0){
        	$("#divMessage").show();
			$("#spanMessage").text('Debe ingresar un usuario');
        }else if( pass.length < 8 ){
			$("#divMessage").show();
			$("#spanMessage").text('La Contraseña debe ser de al menos 8 caracteres');
		}else if( pass != verPass ){
			$("#divMessage").show();
			$("#spanMessage").text('Verifique que su Contraseña conicida');
		}else{
			var data = {
	    			user: user,
	    			password: pass,
	    			tipoUsuario: userType
	    	};
	        
	        var urlPost = "altaUsuario.do";

	    	$.ajax({
	    		type: 'POST',
	    		url: urlPost,
	    		dataType: "json",
	    		data: JSON.stringify(data), 
	    		contentType: 'application/json',
	    		mimeType: 'application/json',
	    		beforeSend: function(xhr) {
	    			xhr.overrideMimeType('application/json; charset=iso-8859-1');
	    		},
	    		success: function(resp) {
	    			if(resp.codigo == "OK"){
	    				$("#divMessage").show();
	    				$("#spanMessage").text('Usuario registrado con exito');
	    				$("#divMessage").removeClass( "alert-danger" ).addClass( "alert-success" );
	    			}else{
	    				$("#divMessage").show();
	    				$("#spanMessage").text('Error: ' + resp.codigo + '. ' + resp.mensaje);
	    				$("#divMessage").removeClass( "alert-success" ).addClass( "alert-danger" );
	    			}
	    		},
	    		error: function (err) {
	    			console.log(err);
	    		},complete: function(){
	    			Metronic.stopPageLoading();
	    		}
	    	});
	   }
	});
});
$(document).ready(function() {
	$('#eliminar').click(function(){
		actualizarUsuario("eliminar");
	    
	});
	
	$('#bloquear').click(function(){
		actualizarUsuario("bloquear");
	    
	});
	
	$('#desbloquear').click(function(){
		actualizarUsuario("desbloquear");
	    
	});
	
});
function actualizarUsuario(operacion)
{
	var opcion = $( "input:radio[name=opcion]:checked" ).val();
	
	if(opcion == null){
		alert("Es necesario que elija un usuario.")
	} else {
		
		
		if (confirm("\u00BFDesea "+ operacion + " el usuario " + opcion + "?")){
			var actionURL = "/MXConectados/" + operacion + "Usuario.do?opcion=" + opcion;
			
			$('#formUsuarios').attr('action', actionURL);
	        $("#formUsuarios").submit();
		} else {
			return false;
		}
	}
}