function loadDashboardResumeTable(gid,estado) {
	var urlPost = "consultaDashboard.do";

	var data = {
			gid: gid,
			estado: estado
	};

	$.ajax({
		//async: false,
		type: 'POST',
		url: urlPost,
		dataType: "json",
		data: JSON.stringify(data), 
		contentType: 'application/json',
		mimeType: 'application/json',
		beforeSend: function(xhr) {
			xhr.overrideMimeType('application/json; charset=iso-8859-1');
		},
		success: function(resp) {
			/*var r = [];
			for( var i = 0; i < 10; i++){
				r.push(resp[i]);
			}*/
			drawResumeTable(resp);
		},
		error: function (err) {
			console.log(err);
		},complete: function(){
			//Metronic.stopPageLoading();
			
		}
	});
}

function drawResumeTable(resp){
	if ( !$.fn.dataTable.isDataTable( '#dashboardResumeTable' ) ) {
		TableAdvanced.init();
	}

	$('#tabla_resumen_body').empty();
	var table = $('#dashboardResumeTable').DataTable();
	table.clear();
	
	if(resp.rows.length > 0) {
		$.each(resp.rows, function(i) {
			var trEstadoServicio = '';
			switch(this.estadoServicio) {
			case 0:
				trEstadoServicio = '<span class="label label-sm label-warning">No detectado</span>'
					break;
			case 1:
				trEstadoServicio = '<span class="label label-sm label-success">En linea</span>'
					break;
			case 2:
				trEstadoServicio = '<span class="label label-sm label-danger">Desconectado</span>'
					break;
			case 3:
				trEstadoServicio = '<span class="label label-sm label-danger">Invalido</span>'
					break;
			}

			table.row.add([
			               this.gID,
			               this.fechaInstSitioAxtel,
			               this.fechaFirmaActaInicioOper,
			               this.anchoBandaSubida,
			               this.anchoBandaBajada,
			               this.ipHomologada,
			               this.estadoDelServicio,
			               this.trafico,
			               this.entidadFederativa,
			               this.municipio,
			               this.localidad,
			               this.institucion,
			               this.tasaTransferenciaSubida,
			               this.tasaTransferenciaBajada,
			               this.latencia
			               ]).draw();
			table.row.onclick = setupDashboardResumeTableClicks();
			return i<resp.rows.length;
		});
	}
	setupDashboardResumeTableClicks();
}


function setupDashboardResumeTableClicks() {
	var oTable = $('#dashboardResumeTable').dataTable();
	
	$('#dashboardResumeTable tbody tr').click( function () {
		
		Metronic.startPageLoading({animate: true});
		Metronic.blockUI({
	        boxed: true,
	        message: 'Cargando...'
	    });

		$("#moreUsedProtocolsTableDiv").show();
		$("#moreUsedProtocolsChartDiv").show();
		$("#bandWidthDiv").show();
		$("#transferRateChartDiv").show();
		$("#latenciChartDiv").show();

		$("#moreUsedProtocolsTablePortletBodyDiv").show();
		$("#moreUsedProtocolsChartPortletBodyDiv").show();
		$("#bandWidthPortletBodyDiv").show();
		$("#transferRateChartPortletBodyDiv").show();
		$("#latenciChartPortletBodyDiv").show();

		var aData = oTable.fnGetData( this );
		var selectedGid = aData[0];

		
		var data = {
				gid: selectedGid,
		};
		
		var urlPost = "consultaDatosProtocolo.do";
		
		$.ajax({
			type: 'POST',
			url: urlPost,
			dataType: "json",
			data: JSON.stringify(data), 
			contentType: 'application/json',
			mimeType: 'application/json',
			beforeSend: function(xhr) {
				xhr.overrideMimeType('application/json; charset=iso-8859-1');
			},
			success: function(resp) {
				drawGidProtocolsChart(resp);
			},
			error: function (err) {
				console.log(err);
			},complete: function(){
				Metronic.stopPageLoading();
				Metronic.unblockUI();
			}
		});
		
		urlPost = "consultaDatosAnchoBanda.do";
		
		$.ajax({
			type: 'POST',
			url: urlPost,
			dataType: "json",
			data: JSON.stringify(data), 
			contentType: 'application/json',
			mimeType: 'application/json',
			beforeSend: function(xhr) {
				xhr.overrideMimeType('application/json; charset=iso-8859-1');
			},
			success: function(resp) {
				drawGidBandwidthChart(resp);
			},
			error: function (err) {
				console.log(err);
			},complete: function(){
				Metronic.stopPageLoading();
				Metronic.unblockUI();
			}
		});
		
		
		urlPost = "consultaDatosTasaTransferencia.do";
		$.ajax({
			type: 'POST',
			url: urlPost,
			dataType: "json",
			data: JSON.stringify(data), 
			contentType: 'application/json',
			mimeType: 'application/json',
			beforeSend: function(xhr) {
				xhr.overrideMimeType('application/json; charset=iso-8859-1');
			},
			success: function(resp) {
				drawGidTransferRate(resp);
			},
			error: function (err) {
				console.log(err);
			},complete: function(){
				Metronic.stopPageLoading();
				Metronic.unblockUI();
			}
		});
		
		urlPost = "consultaDatosLatencia.do";
		$.ajax({
			type: 'POST',
			url: urlPost,
			dataType: "json",
			data: JSON.stringify(data), 
			contentType: 'application/json',
			mimeType: 'application/json',
			beforeSend: function(xhr) {
				xhr.overrideMimeType('application/json; charset=iso-8859-1');
			},
			success: function(resp) {
				drawGidLatencyChart(resp);
			},
			error: function (err) {
				console.log(err);
			},complete: function(){
				Metronic.stopPageLoading();
				Metronic.unblockUI();
			}
		});
		
		
		
		Metronic.unblockUI();
	});
}

function drawGidBandwidthChart(data){
	var chartData = [];
	
	for (var i = 0; i < data.length; i++) {
		var newDate = parseDate(data[i].time);
		chartData.push({			
			time: data[i].time,
			downBytes: data[i].downBytes
		});
	}
	initBandWidthChart(chartData);
}

function drawGidTransferRate(data){
	var chartData = [];
	
	for (var i = 0; i < data.length; i++) {
		var newDate = parseDate(data[i].time);
		chartData.push({
			time: newDate,
			transferRateIn: data[i].transferRateIn
		});
	}
	initTransferRateChart(chartData);
}

function drawGidLatencyChart(data){
	var chartData = [];
	
	for (var i = 0; i < data.length; i++) {
		var newDate = parseDate(data[i].time);
		chartData.push({
			time: newDate,
			latency: data[i].latency
		});
	}
	
	initLatencyChart(chartData);
}

function drawGidProtocolsChart(data){
var chartData = [];
var tabData = data;
	
	for (var i = 0; i < 10 && i < data.length; i++) {
		//var newDate = parseDate(data[i].time);
		chartData.push({			
			protocol: data[i].protocol,
			total: data[i].total
		});
		
	}
	//Metronic.stopPageLoading();
	initMoreUsedProtocolsChart(chartData);

	if ( $.fn.dataTable.isDataTable( '#moreUsedProtocolsTable' ) ) {
		var table = $('#moreUsedProtocolsTable').DataTable();
	}

	$('#tabla_protocolos_body').empty();
	if ( !$.fn.dataTable.isDataTable( '#moreUsedProtocolsTable' ) ) {
		$.extend(true, $.fn.DataTable.TableTools.classes, {
			"container": "btn-group tabletools-dropdown-on-portlet",
			"buttons": {
				"normal": "btn btn-sm default",
				"disabled": "btn btn-sm default disabled"
			},
			"collection": {
				"container": "DTTT_dropdown dropdown-menu tabletools-dropdown-menu"
			}
		});

		var table = $('#moreUsedProtocolsTable').DataTable({
			"paging":   false,
			"ordering": false,
			"info":     false,
			"searching": false,
			"dom": 'T',
		} );
	}

	var table = $('#moreUsedProtocolsTable').DataTable();
	table.clear();
	for (var i = 0; i < 10 && i < tabData.length; i++) {
		table.row.add([
		               tabData[i].protocol,
		               tabData[i].total,
		               tabData[i].percent
		               ]).draw();
	}

	//Metronic.stopPageLoading();
	initMoreUsedProtocolsChart(tabData);
}

function parseDate(input) {
	  var timeParts = input.split(' ');
	  var day = timeParts[0];
	  var hours = timeParts[1];
	  var dayParts = day.split('-');
	  var hoursParts = hours.split(':');
	  return new Date(dayParts[2], dayParts[1]-1, dayParts[0], hoursParts[0], hoursParts[1], hoursParts[2] );
}



$(document).ready(function() {
	/*Metronic.blockUI({
        boxed: true,
        message: 'Cargando...'
    });*/
	
	if ( !$.fn.dataTable.isDataTable( '#dashboardResumeTable' ) ) {
		TableAdvanced.init();
	}

	var state = $( "[id='1']" ).addClass("active");
	$( "#stateText" ).text( "Resumen Aguascalientes" );

	$("#formGid").submit(function (e) {
		e.preventDefault();
		var gid = $("#searchByGID").val();
		loadDashboardResumeTable(gid, "");
	});

	$( "#searchByGIDButton" ).click(function( event ) {
		var gid = $("#searchByGID").val();
		loadDashboardResumeTable(gid, "");
	});

	$("#statesButtons").find("li").click(function () {
		//Metronic.startPageLoading({animate: true});
		/*Metronic.blockUI({
	        boxed: true,
	        message: 'Cargando...'
	    });*/

		$("#moreUsedProtocolsTableDiv").hide();
		$("#moreUsedProtocolsChartDiv").hide();
		$("#bandWidthDiv").hide();
		$("#transferRateChartDiv").hide();
		$("#latenciChartDiv").hide();

		$("#moreUsedProtocolsTablePortletBodyDiv").hide();
		$("#moreUsedProtocolsChartPortletBodyDiv").hide();
		$("#bandWidthPortletBodyDiv").hide();
		$("#transferRateChartPortletBodyDiv").hide();
		$("#latenciChartPortletBodyDiv").hide();

		var state = $( "[id='" + this.id + "']" );
		$( "#statesButtons" ).find( "li" ).removeClass("active");
		$( "#stateText" ).text( "Resumen " + state.text() );
		state.addClass("active");
		console.log(this.id);
		loadDashboardResumeTable("", this.id);
		
		//Metronic.unblockUI();
	});
	
	//Metronic.unblockUI();
	setupDashboardResumeTableClicks();
	
	$(document).ajaxStart(Metronic.blockUI()).ajaxStop(Metronic.unblockUI());
});