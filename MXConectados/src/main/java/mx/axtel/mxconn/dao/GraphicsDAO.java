package mx.axtel.mxconn.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import mx.axtel.mxconn.beans.BandWidthBean;
import mx.axtel.mxconn.beans.LatencyBean;
import mx.axtel.mxconn.beans.ProtocolBean;
import mx.axtel.mxconn.beans.TransferRateBean;
import mx.axtel.mxconn.exceptions.SQLOperationException;
import mx.axtel.mxconn.utils.ServiceLocator;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * The Class GraphicsDAO.
 * 
 * @author Luigi
 */
@Repository
public class GraphicsDAO {

	@Autowired
	private ServiceLocator locator;

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(GraphicsDAO.class);
	
	private static final String SQL_BAND_WIDTH = "SELECT DATEADD(S,AFS.STATISTICTIME,'1970-01-01 00:00:00') as time, AFS.DOWN_BYTES/1000000 FROM elog.dbo.VW_APP_FLOW_STAT AFS WHERE AFS.DEVICENAME = ? AND DATEADD(S,AFS.STATISTICTIME,'1970-01-01 00:00:00') > GETDATE() - 1 ORDER BY AFS.STATISTICTIME";
	
	private StringBuilder sql_transfer = new StringBuilder()
		.append("Select  I.time,I.DISPLAYVALUE, I.IFHCOUTOCTETSSPEED, I.IFHCINOCTETSSPEED ")
		.append("from NEMGR.VIEW_INV_NE INE, ")
		.append("(SELECT to_char(to_date('01-01-1970 00:00:00','DD-MM-YYYY HH24:MI:SS') ")
		.append(" + TIMESTAMP/86400000,'DD-MM-YYYY HH24:MI:SS') as time")
		.append(",DISPLAYVALUE, DN ,IFHCOUTOCTETSSPEED,IFHCINOCTETSSPEED ")
		.append("FROM OMSPM.VIEW_IFXTRAFFICSTAT ) I ")
		.append("where INE.NENAME = ?")
		.append(" and INE.NEDN = I.DN ")
		.append("AND to_date(TO_CHAR( SYSDATE - (24/24),'DD-MM-YYYY HH24:MI:SS'),'DD-MM-YYYY HH24:MI:SS') ")
		.append("<= to_date(I.time,'DD-MM-YYYY HH24:MI:SS') order by I.time");
	
	private StringBuilder sql_latency = new StringBuilder()
		.append("SELECT N.NEPINGRESPTIME ,  N.time FROM NEMGR.VIEW_INV_NE INE, ")
		.append("(SELECT NEPINGRESPTIME, DN,to_char(to_date('01-01-1970 00:00:00','DD-MM-YYYY HH24:MI:SS') ")
		.append("+ TIMESTAMP/86400000,'DD-MM-YYYY HH24:MI:SS') time ")
		.append("FROM OMSPM.VIEW_NEPINGRESPTIMESTAT) N WHERE INE.NENAME = ? ")
		.append("AND to_date(TO_CHAR( SYSDATE - (24/24),'DD-MM-YYYY HH24:MI:SS'),'DD-MM-YYYY HH24:MI:SS') ")
		.append("<= to_date(N.time,'DD-MM-YYYY HH24:MI:SS')")
		.append("AND INE.NEDN = N.DN ORDER BY N.time asc");
	
	
	private StringBuilder sql_protocolo = new StringBuilder()
		.append("SELECT PP.PROTO,SUM(TOTAL) AS TOTAL, ")
		.append("TRUNC((100*SUM(TOTAL)/SS.SUMA),2) AS PORCENT ")
		.append("FROM (SELECT CASE ")
		.append("WHEN PRO.PROTOCOLO IS NULL OR PRO.PROTOCOLO = 'HTTP' THEN 'HTTP' ")
		.append("ELSE PRO.PROTOCOLO ")
		.append("END AS PROTO, AFS.TOTAL ")
		.append("FROM COUNT_PROTOCOL AFS, PROTOCOLOS PRO ")
		.append("WHERE AFS.DEVICENAME = ? ")
		.append("AND PRO.APP_NAME (+)= AFS.APP_NAME) PP, ")
		.append("(SELECT SUM(TOTAL) AS SUMA FROM COUNT_PROTOCOL) SS ")
		.append("GROUP BY PP.PROTO, SS.SUMA ORDER BY TOTAL DESC");

	
	/**
	 * Instantiates a new GID operations.
	 */
	public GraphicsDAO() {
		LOG.debug("Loading GraphicsDAO");
	}

	/**
	 * Get all information from Band Width, using GID
	 * @param key
	 * @return List of Band Width by GID
	 * @throws SQLOperationException
	 * @throws SQLException 
	 */
	public List<BandWidthBean> getAllBandWidthData(String key) throws SQLOperationException, SQLException {
		TimeZone timeZone = TimeZone.getTimeZone("America/Mexico_City");
		Locale locale = new Locale("es", "MX");
		Connection connection = locator.getConnectionSqlServer();
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<BandWidthBean> listOfRecords = null;
		BandWidthBean record = null;
		try {
			rs = executeSQL(connection, pst, rs, key, GraphicsEnum.BAND_WIDTH);
			if (rs != null) {
				listOfRecords = new ArrayList<BandWidthBean>();
				while (rs.next()) {
					record = new BandWidthBean();
					record.setGid(key);
					
					record.setTime(rs.getString(1));
					record.setDownBytes(rs.getString(2));
					listOfRecords.add(record);
				}
			}
		} catch (SQLException e) {
			throw new SQLOperationException(e);
		} finally {
			try {
				if (rs != null) rs.close();
				if (pst != null) pst.close();
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				throw new SQLOperationException(e);
			}
		}
		return listOfRecords;
	}
	
	/**
	 * Get all information from Transfer Rate, using GID
	 * @param key
	 * @return List of Transfer Rate by GID
	 * @throws SQLOperationException
	 * @throws SQLException 
	 */
	public List<TransferRateBean> getAllTransferRateData(String key) throws SQLOperationException, SQLException {
		TimeZone timeZone = TimeZone.getTimeZone("America/Mexico_City");
		Locale locale = new Locale("es", "MX");
		Connection connection = locator.getConnectionAxtel();
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<TransferRateBean> listOfRecords = null;
		TransferRateBean record = null;
		try {
			rs = executeSQL(connection, pst, rs, key, GraphicsEnum.TRANSFER_RATE);
			LOG.debug(key);
			if (rs != null) {
				listOfRecords = new ArrayList<TransferRateBean>();
				while (rs.next()) {
					record = new TransferRateBean();
					record.setGid(key);					
					record.setTime(rs.getNString("TIME"));
					record.setDisplayValue(rs.getNString("DISPLAYVALUE"));
					record.setTransferRateOut(rs.getNString("IFHCOUTOCTETSSPEED"));
					record.setTransferRateIn(rs.getNString("IFHCINOCTETSSPEED"));
					listOfRecords.add(record);
				}
			}
		} catch (SQLException e) {
			throw new SQLOperationException(e);
		} finally {
			try {
				if (rs != null) rs.close();
				if (pst != null) pst.close();
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				throw new SQLOperationException(e);
			}
		}
		return listOfRecords;
	}
	
	/**
	 * Get all information from Latency, using GID
	 * @param key
	 * @return List of Latency by GID
	 * @throws SQLOperationException
	 * @throws SQLException 
	 */
	public List<LatencyBean> getAllLatencyData(String key) throws SQLOperationException, SQLException {
		TimeZone timeZone = TimeZone.getTimeZone("America/Mexico_City");
		Locale locale = new Locale("es", "MX");
		Connection connection = locator.getConnectionAxtel();
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<LatencyBean> listOfRecords = null;
		LatencyBean record = null;
		try {
			rs = executeSQL(connection, pst, rs, key, GraphicsEnum.LATENCY);
			if (rs != null) {
				listOfRecords = new ArrayList<LatencyBean>();
				while (rs.next()) {
					record = new LatencyBean();
					record.setGid(key);
					record.setTime(rs.getNString("time"));
					record.setLatency(rs.getNString("NEPINGRESPTIME"));
					listOfRecords.add(record);
				}
			}
		} catch (SQLException e) {
			throw new SQLOperationException(e);
		} finally {
			try {
				if (rs != null) rs.close();
				if (pst != null) pst.close();
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				throw new SQLOperationException(e);
			}
		}
		return listOfRecords;
	}
	
	/**
	 * Get all information from protocols, using GID
	 * @param key
	 * @return List of protocols by GID
	 * @throws SQLOperationException
	 * @throws SQLException 
	 */
	public List<ProtocolBean> getAllProtocolData(String key) throws SQLOperationException, SQLException {
		
		TimeZone timeZone = TimeZone.getTimeZone("America/Mexico_City");
		Locale locale = new Locale("es", "MX");
		Connection connLocal = locator.getConnection();
		PreparedStatement pstLocal = null;
		ResultSet rs = null;
		int count = 0;
		
		List<ProtocolBean> listOfRecords = null;
		ProtocolBean record = null;
		insertProtocol(connLocal, pstLocal, key);
		try {
			System.out.println("sql_protocolo: " + sql_protocolo);
			pstLocal = connLocal.prepareStatement(sql_protocolo.toString());
			pstLocal.setString(1,key);
			rs = pstLocal.executeQuery();
			if (rs != null) {
				listOfRecords = new ArrayList<ProtocolBean>();
				while (rs.next() && count<10) {
					record = new ProtocolBean();
					record.setGid(key);
					record.setProtocol(rs.getNString("PROTO"));
					record.setTotal(rs.getNString("TOTAL"));
					record.setPercent(rs.getNString("PORCENT"));
					listOfRecords.add(record);
					count++;
				}
			}
			
			
		} catch (SQLException e) {
			throw new SQLOperationException(e);
		} finally {
			try {
				if (rs != null) rs.close();
				if (pstLocal != null) pstLocal.close();
				if (connLocal != null)
					connLocal.close();
			} catch (SQLException e) {
				throw new SQLOperationException(e);
			}
		}
		return listOfRecords;
	}
	
	private void insertProtocol(Connection connLocal, PreparedStatement pst, String key) 
			throws SQLOperationException, SQLException{
		StringBuilder deleteSQL = new StringBuilder()
		.append(" DELETE COUNT_PROTOCOL");
		
		StringBuilder sqlAppFlow = new StringBuilder()
		.append("SELECT AFS.DEVICENAME, AFS.APP_NAME, COUNT(1) AS TOTAL")
		.append(" FROM elog.dbo.VW_APP_FLOW_STAT AFS")
		.append(" WHERE AFS.DEVICENAME = ? AND DATEADD(S,AFS.STATISTICTIME,'1970-01-01 00:00:00') > GETDATE() - 1")
		.append(" GROUP BY AFS.DEVICENAME, AFS.APP_NAME");
		
		StringBuilder sqlInsert = new StringBuilder()
		.append("INSERT INTO COUNT_PROTOCOL (DEVICENAME,APP_NAME,TOTAL) VALUES ")
		.append("(?,?,?)");
		TimeZone timeZone = TimeZone.getTimeZone("America/Mexico_City");
		Locale locale = new Locale("es", "MX");
		Connection connection = locator.getConnectionSqlServer();
		
		PreparedStatement pstLocal = null;
		ResultSet rs = null;
		
		List<ProtocolBean> listOfRecords = null;
		ProtocolBean record = null;
		try{
			pstLocal = connLocal.prepareStatement(deleteSQL.toString());
			pstLocal.executeUpdate();	
			pst = connection.prepareStatement(sqlAppFlow.toString());
			pstLocal = connLocal.prepareStatement(sqlInsert.toString());
			pst.setString(1, key);
			rs = pst.executeQuery();
			
			while (rs.next()) {
				listOfRecords = new ArrayList<ProtocolBean>();
				pstLocal.setString(1,key);
				pstLocal.setString(2, rs.getString("APP_NAME"));
				pstLocal.setInt(3, rs.getInt("TOTAL"));
				pstLocal.executeUpdate();	
			}
			connLocal.commit();
		} catch (SQLException e) {
			throw new SQLOperationException(e);
		} finally {
			try {
				if (rs != null) rs.close();
				if (pst != null) pst.close();
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				throw new SQLOperationException(e);
			}
		}
	}
	
	private ResultSet executeSQL(Connection connection, PreparedStatement pst, ResultSet rs, String key, GraphicsEnum query) throws SQLOperationException {
		String sql = null;
		try {
			switch (query) {
			case BAND_WIDTH:
				sql = SQL_BAND_WIDTH;
				break;
			case TRANSFER_RATE:
				sql = sql_transfer.toString();
				break;
			case LATENCY:
				sql = sql_latency.toString();
				break;
			case PROTOCOL:
				sql = sql_protocolo.toString();
				break;
			default:
				sql = SQL_BAND_WIDTH;
				break;
			}
			System.out.println("query.....: " + query);
			System.out.println("sqlgrafica: " + sql);
			
			pst = connection.prepareStatement(sql);
			pst.setString(1, key);
			rs = pst.executeQuery();
		} catch (SQLException e) {
			throw new SQLOperationException(e);
		}
		return rs;
	}

}
