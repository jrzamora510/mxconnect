package mx.axtel.mxconn.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mx.axtel.mxconn.beans.Usuario;
import mx.axtel.mxconn.exceptions.SQLOperationException;
import mx.axtel.mxconn.utils.ServiceLocator;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * The Class UsuariosDAO.
 */
@Repository
public class UsuariosDAO {
	
	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(UsuariosDAO.class);
	
	private static final String UPDATE_PASSWORD = "UPDATE USERS SET PASSWORD = ? WHERE USERNAME = ?";

	/** The Constant SELECT_PASSWORD. */
	private static final String SELECT_PASSWORD = "SELECT PASSWORD FROM USERS WHERE USERNAME = ?";

	/** The Constant INSERT_USERS. */
	private static final String INSERT_USERS = "INSERT INTO USERS(USERNAME,PASSWORD,ENABLED) VALUES (?,?,1)";

	/** The Constant INSERT_AUTHORITIES. */
	private static final String INSERT_AUTHORITIES = "INSERT INTO AUTHORITIES (USERNAME, AUTHORITY) VALUES (?, ?) ";
	
	private StringBuilder slqSelectAll = new StringBuilder().append( "SELECT U.USERNAME, U.ENABLED, A.AUTHORITY FROM USERS U, AUTHORITIES A WHERE U.USERNAME = A.USERNAME");
	
	private StringBuilder slqElimina = new StringBuilder().append( "DELETE USERS WHERE USERNAME = ?");
	
	private StringBuilder slqEliminaAut = new StringBuilder().append( "DELETE AUTHORITIES WHERE USERNAME = ?");
	
	private StringBuilder slqBloquea = new StringBuilder().append( "UPDATE USERS SET ENABLED=? WHERE USERNAME = ?");
	
	
	/** The service locator. */
	@Autowired
	ServiceLocator serviceLocator;

	/**
	 * Elimina un usuario de la base de datos
	 * @param idUser identificador del usuario
	 * @return registros afectados
	 * @throws SQLOperationException
	 * @throws SQLException 
	 */
	public int eliminarUsuario(String idUser) throws SQLOperationException, SQLException {
		int iResp = 0;
		PreparedStatement ps = null;
		PreparedStatement psAut = null;
		Connection conn = null;
		LOG.debug(slqEliminaAut);
		LOG.debug(slqElimina);
		LOG.debug("idUser = " + idUser);
		try {
			conn = serviceLocator.getConnection();
			ps = conn.prepareStatement(slqEliminaAut.toString());
			ps.setString(1, idUser);
			iResp = ps.executeUpdate();
			
			psAut = conn.prepareStatement(slqElimina.toString());
			psAut.setString(1, idUser);
			iResp = psAut.executeUpdate();
			
			conn.commit();
		} catch (SQLException e) {
			conn.rollback();
			throw new SQLOperationException(e);
		} finally {
			try {
				if (ps != null) ps.close();
				if (psAut != null) psAut.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				throw new SQLOperationException(e);
			}
		}
		return iResp;
	}
	
	/**
	 * Bloquea el acceso de un usuario al aplicativo
	 * @param idUser identificador del usuario
	 * @return numero de registros afectados
	 * @throws SQLOperationException
	 */
	public int bloquearUsuario(String idUser, int accion) throws SQLOperationException {
		int iResp = 0;
		PreparedStatement ps = null;
		Connection conn = null;
		try {
			conn = serviceLocator.getConnection();
			ps = conn.prepareStatement(slqBloquea.toString());
			ps.setInt(1, accion);
			ps.setString(2, idUser);
			iResp = ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLOperationException(e);
		} finally {
			try {
				if (ps != null) ps.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				throw new SQLOperationException(e);
			}
		}
		return iResp;
	}
	
	/**
	 * Consulta de todos los usuarios
	 * @return Lista de usuarios
	 * @throws SQLOperationException
	 */
	public List<Usuario> consultarUsuarios () throws SQLOperationException {
		List<Usuario> list = new ArrayList<Usuario>();
		Usuario usuario = null;
		PreparedStatement ps = null;
		Connection conn = null;
		ResultSet rs = null;
		try {
			conn = serviceLocator.getConnection();
			ps = conn.prepareStatement(slqSelectAll.toString());
			rs = ps.executeQuery();
			while(rs.next()) {
				usuario = new Usuario();
				usuario.setUser(rs.getString("USERNAME"));
				usuario.setBloqueado(rs.getInt("ENABLED"));
				usuario.setTipoUsuario(rs.getString("AUTHORITY"));
				list.add(usuario);
			}
		} catch (SQLException e) {
			throw new SQLOperationException(e);
		} finally {
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				throw new SQLOperationException(e);
			}
		}
		return list;
	}
	
	/**
	 * Insertar usuario.
	 * @param usr the usr
	 * @throws SQLOperationException the SQL operation exception
	 */
	public void insertarUsuario(Usuario usr) throws SQLOperationException {
		PreparedStatement ps = null;
		Connection conn = null;
		
		try {
			conn = serviceLocator.getConnection();
			conn.setAutoCommit(false);

			ps = conn.prepareStatement(INSERT_USERS);
			ps.setString(1, usr.getUser());
			ps.setString(2, usr.getPassword());
			ps.executeUpdate();
			
			ps = conn.prepareStatement(INSERT_AUTHORITIES);
			ps.setString(1, usr.getUser());
			ps.setString(2, usr.getTipoUsuario());
			ps.executeUpdate();
			
			conn.commit();
		} catch (SQLException e) {
			throw new SQLOperationException(e);
		} finally {
			try {
				if (ps != null) ps.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				throw new SQLOperationException(e);
			}
		}
	}

	/**
	 * Consulta password actual.
	 * @param usr the usr
	 * @return the string
	 * @throws SQLOperationException the SQL operation exception
	 */
	public String consultaPasswordActual(String usr) throws SQLOperationException {
		String hashedPwd = "";
		PreparedStatement ps = null;
		Connection conn = null;
		ResultSet rs = null;
		try {
			conn = serviceLocator.getConnection();
			ps = conn.prepareStatement(SELECT_PASSWORD, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
			ps.setString(1, usr);
			rs = ps.executeQuery();
			if(rs.first()) {
				hashedPwd = rs.getString(1);
			}
		} catch (SQLException e) {
			throw new SQLOperationException(e);
		} finally {
			try {
				if (ps != null) ps.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				throw new SQLOperationException(e);
			}
		}
		return hashedPwd;
	}

	/**
	 * Actualiza password.
	 * @param usr the usr
	 * @throws SQLOperationException the SQL operation exception
	 */
	public void actualizaPassword(Usuario usr) throws SQLOperationException {
		PreparedStatement ps = null;
		Connection conn = null;
		
		try {
			conn = serviceLocator.getConnection();
			ps = conn.prepareStatement(UPDATE_PASSWORD);
			ps.setString(1, usr.getNuevoPassword());
			ps.setString(2, usr.getUser());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLOperationException(e);
		} finally {
			try {
				if (ps != null) ps.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				throw new SQLOperationException(e);
			}
		}
	}
}
