package mx.axtel.mxconn.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mx.axtel.mxconn.entity.Gid;
import mx.axtel.mxconn.exceptions.SQLOperationException;
import mx.axtel.mxconn.utils.ServiceLocator;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * The Class GIDOperations.
 * @author Luigi
 */
@Repository
public class GIDOperations implements SQLOperations {

	@Autowired
	private ServiceLocator locator;
	
	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(GIDOperations.class);
	
	/**
	 * Instantiates a new GID operations.
	 */
	public GIDOperations() {
		LOG.debug("Loading GIDOperations");
	}

	/* (non-Javadoc)
	 * @see mx.axtel.mxconn.dao.SQLOperations#searchByPrimaryKey(java.lang.Object)
	 */
	@Override
	public List<Gid> searchByPrimaryKey(String pk) throws SQLOperationException {
		List<Gid> listOfRecords = null;
		StringBuilder sql = new StringBuilder();
		sql.append(getSQLBase());
		sql.append("WHERE GID = ?");
		LOG.debug("SQL Statement [" + sql.toString() + "]");
		listOfRecords = searchBy(sql.toString(), pk);
		LOG.debug("Records founds [" + listOfRecords.size() + "]");
		return listOfRecords;
	}

	/* (non-Javadoc)
	 * @see mx.axtel.mxconn.dao.SQLOperations#searchByIndexColum(java.lang.Object)
	 */
	@Override
	public List<Gid> searchByIndexColum(String idx) throws SQLOperationException {
		List<Gid> listOfRecords = null;
		StringBuilder sql = new StringBuilder();
		sql.append(getSQLBase());
		sql.append("WHERE NOMBRE_DEL_ESTADO = ?");
		LOG.debug("SQL Statement [" + sql.toString() + "]");
		listOfRecords = searchBy(sql.toString(), idx);
		LOG.debug("Records founds [" + listOfRecords.size() + "]");
		return listOfRecords;
	}
	
	/**
	 * Global Method to search records by one or more parameters.
	 *
	 * @param sql the sql
	 * @param params the params
	 * @return List
	 * @throws SQLOperationException the SQL operation exception
	 */
	private List<Gid> searchBy(String sql, String... params) throws SQLOperationException {
		java.sql.Connection connection = null;
		java.sql.PreparedStatement pst = null;
		java.sql.ResultSet rs = null;
		List<Gid> listOfRecords = null;
		int i=1;
		try {
			connection = locator.getConnection();
			pst = connection.prepareStatement(sql.toString());
			for (String p: params) {
				pst.setString(i, p);
				i++;
			}
			rs = pst.executeQuery();
			listOfRecords = buildList(rs);
		} catch (SQLException e) {
			throw new SQLOperationException(e);
		} finally {
			try {
				if (connection != null) connection.close();
			} catch (SQLException e) {
				throw new SQLOperationException(e);
			}
		}
		return listOfRecords;
	}
	
	/**
	 * Get principal SQL for GID_PIVOTE table.
	 *
	 * @return SQL Base
	 */
	private String getSQLBase() {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT ");
		sql.append("GID, INSTITUCION, NOMBRE_DEL_ESTADO, ORDER_MODEM,");
		sql.append("ORDEN_IP_FIJA, IP_FIJA, ESTATUS, FECHA, IP_Fija_en_e_Sight,");
		sql.append("Validacion_IP, NOTAS, SITIO_GID, FOLIO, RECEPTOR, ");
		sql.append("Fecha_Firma_Acta_Inicio_Oper, No_Serie_USG, Mac_Addres,");
		sql.append("Direccion_IP_Homologada, Comunidad_SNMP, IP_Oficio_SCT,");
		sql.append("Alta_CIMOV, Fecha_notificado_CSIC, Autorizado_CSIC_MENOR_20_dias,");
		sql.append("PROVEEDOR_MT, FECHA_PRUEBAS, SUSPENDIDOS_SCT_OFICIO ");
		sql.append("FROM GID_PIVOTE ");
		return sql.toString();
	}
	
	/**
	 * create a List of records from GID_PIVOTE table.
	 *
	 * @param rs the rs
	 * @return List
	 * @throws SQLException the SQL exception
	 */
	private List<Gid> buildList(java.sql.ResultSet rs) throws SQLException {
		Gid record = null;
		List<Gid> listOfRecords = new ArrayList<Gid>();
		while (rs.next()) {
			record = new Gid();
			record.setGid(rs.getNString(1));
			record.setInstitucion(rs.getNString(2));
			record.setNombreDelEstado(rs.getNString(3));
			record.setOrderModem(rs.getNString(4));
			record.setOrdenIpFija(rs.getNString(5));
			record.setIpFija(rs.getNString(6));
			record.setEstatus(rs.getNString(7));
			record.setFecha(rs.getNString(8));
			record.setIpFijaEneSight(rs.getNString(9));
			record.setValidacionIp(rs.getNString(10));
			record.setNotas(rs.getNString(11));
			record.setSitioGid(rs.getNString(12));
			record.setFolio(rs.getNString(13));
			record.setReceptor(rs.getNString(14));
			record.setFechaFirmaActaInicioOper(rs.getNString(15));
			record.setNoSerieUSG(rs.getNString(16));
			record.setMacAddres(rs.getNString(17));
			record.setDireccionIPHomologada(rs.getNString(18));
			record.setComunidadSNMP(rs.getNString(19));
			record.setIpOficioSCT(rs.getNString(20));
			record.setAltaCIMOV(rs.getNString(21));
			record.setFechanotificadoCSIC(rs.getNString(22));
			record.setAutorizadoCSICMenor20Dias(rs.getNString(23));
			record.setProveedorMt(rs.getNString(24));
			record.setFechaPruebas(rs.getNString(25));
			record.setSuspendidoSCTOficio(rs.getNString(26));
			listOfRecords.add(record);
		}
		return listOfRecords;
	}

}
