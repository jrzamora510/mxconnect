package mx.axtel.mxconn.dao;

import java.sql.SQLException;

import mx.axtel.mxconn.beans.DashboardRow;
import mx.axtel.mxconn.exceptions.SQLOperationException;

/**
 * The Interface SQLOperations.
 */
public interface DashboardTableOperations {
	
	/**
	 * Search by primary key.
	 *
	 * @param pk the pk
	 * @return the java.util. list
	 * @throws SQLOperationException the SQL operation exception
	 */
	public java.util.List<DashboardRow> searchByPrimaryKey(String pk) throws SQLOperationException;
	
	/**
	 * Search by index colum.
	 *
	 * @param idx the idx
	 * @return the java.util. list
	 * @throws SQLOperationException the SQL operation exception
	 */
	public java.util.List<DashboardRow> searchByIndexColum(String idx) throws SQLOperationException, SQLException;

}
