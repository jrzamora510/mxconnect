package mx.axtel.mxconn.dao;

import mx.axtel.mxconn.entity.Gid;
import mx.axtel.mxconn.exceptions.SQLOperationException;

/**
 * The Interface SQLOperations.
 */
public interface SQLOperations {
	
	/**
	 * Search by primary key.
	 *
	 * @param pk the pk
	 * @return the java.util. list
	 * @throws SQLOperationException the SQL operation exception
	 */
	public java.util.List<Gid> searchByPrimaryKey(String pk) throws SQLOperationException;
	
	/**
	 * Search by index colum.
	 *
	 * @param idx the idx
	 * @return the java.util. list
	 * @throws SQLOperationException the SQL operation exception
	 */
	public java.util.List<Gid> searchByIndexColum(String idx) throws SQLOperationException;

}
