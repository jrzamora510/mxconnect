/**
 * 
 */
package mx.axtel.mxconn.dao;

/**
 * @author Luigi
 *
 */
public enum GraphicsEnum {
	
	BAND_WIDTH,
	
	TRANSFER_RATE,
	
	LATENCY,
	
	PROTOCOL;

}
