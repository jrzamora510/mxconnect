package mx.axtel.mxconn.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import mx.axtel.mxconn.beans.DashboardRow;
import mx.axtel.mxconn.exceptions.SQLOperationException;
import mx.axtel.mxconn.utils.ServiceLocator;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * The Class DashboardTableDAO.
 * 
 * @author Luigi
 */
@Repository
public class DashboardTableDAO implements DashboardTableOperations {

	@Autowired
	private ServiceLocator locator;

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(DashboardTableDAO.class);
	private Connection connectionSqlServer = null;
	private Connection connectionAxtel = null;
	
	private StringBuilder queryGID = new StringBuilder()
		.append("SELECT GP.GID, GP.FECHA, GP.FECHA_FIRMA_ACTA_INICIO_OPER, ")
		.append("GP.DIRECCION_IP_HOMOLOGADA, E.NOMBRE_ESTADO, GP.MUNICIPIO, ")
		.append("GP.LOCALIDAD, GP.INSTITUCION, GP.ANCHO_SUBIDA/1000000 AS SUBIDA, GP.ANCHO_BAJADA/1000000 AS BAJADA ")
		.append("FROM GID_PIVOTE GP, CAT_ESTADOS E ");
	
	private StringBuilder queryJoins = new StringBuilder()
		.append(" AND GP.CVE_PROYECTO = 1")
		.append(" AND GP.CVE_ESTADO = E.CVE_ESTADO");
	
	private StringBuilder queryJoinsAll = new StringBuilder()
	.append(" WHERE GP.CVE_PROYECTO = 1")
	.append(" AND GP.CVE_ESTADO = E.CVE_ESTADO");

	/**
	 * Instantiates a new GID operations.
	 */
	public DashboardTableDAO() {
		LOG.debug("Loading DashboardTableDAO");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.axtel.mxconn.dao.SQLOperations#searchByPrimaryKey(java.lang.Object)
	 */
	@Override
	public List<DashboardRow> searchByPrimaryKey(String pk) throws SQLOperationException {
			 
		List<DashboardRow> listOfRecords = null;
		StringBuilder sql = new StringBuilder()
			.append(queryGID)
			.append("WHERE GP.GID = ? ")
			.append(queryJoins);
		LOG.debug("SQL Statement [" + sql.toString() + "]");
		listOfRecords = searchBy(sql.toString());
		LOG.debug("Records founds [" + listOfRecords.size() + "]");
		return listOfRecords;
	}
	

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.axtel.mxconn.dao.SQLOperations#searchByIndexColum(java.lang.Object)
	 */
	@Override
	public List<DashboardRow> searchByIndexColum(String idx)
			throws SQLOperationException, SQLException {
		List<DashboardRow> listOfRecords = null;
		StringBuilder sql = new StringBuilder()
		.append(queryGID);
		if(idx.equals("0")) {
			sql.append(queryJoinsAll);
		} else  {
			sql.append("WHERE GP.CVE_ESTADO = ").append(idx)
			.append(queryJoins);
		}
		LOG.debug("SQL Statement [" + sql.toString() + "]");
		listOfRecords = searchBy(sql.toString());
		LOG.debug("Records founds [" + listOfRecords.size() + "]");
		searchOtherElements(listOfRecords);
		return listOfRecords;
	}
	
	private void searchOtherElements(List<DashboardRow> listOfRecords) throws 
										SQLOperationException, SQLException {
		TimeZone timeZone = TimeZone.getTimeZone("America/Mexico_City");
		Locale locale = new Locale("es", "MX");
				
		try{
			
			connectionSqlServer = locator.getConnectionSqlServer();
			connectionAxtel = locator.getConnectionAxtel();
			for (Iterator iterator = listOfRecords.iterator(); iterator.hasNext();) {
				DashboardRow dashboardRow = (DashboardRow) iterator.next();
				searchOracleAxtel(dashboardRow);
				searchSqlServer(dashboardRow);
			}
			
		} finally {
			try {
				if (connectionSqlServer != null)
					connectionSqlServer.close();
				if (connectionAxtel != null)
					connectionAxtel.close();
			} catch (SQLException e) {
				throw new SQLOperationException(e);
			}
		}
	}
	
	private void searchOracleAxtel(DashboardRow row) throws SQLOperationException {
		StringBuilder sql = new StringBuilder()
		.append("SELECT INE.NESTATE, ROUND(AVG(I.IFHCINOCTETSSPEED),2) AS SPEED, ")
		.append(" ROUND(AVG(N.NEPINGRESPTIME),2) AS NEPIN")
		.append(" FROM  NEMGR.VIEW_INV_NE INE, OMSPM.VIEW_IFXTRAFFICSTAT I, OMSPM.VIEW_NEPINGRESPTIMESTAT N ")
		.append(" WHERE CONCAT('USG_', ").append(row.getgID())
		.append(") = INE.NENAME  ")
		.append(" AND INE.NEDN = I.DN(+)")
		.append(" AND INE.NEDN = N.DN(+)")
		.append(" GROUP BY NESTATE");
		//LOG.debug("SQL Statement [" + sql.toString() + "]");
		searchByAxtel(row, sql.toString());

	}
	
	private void searchSqlServer(DashboardRow row) throws SQLOperationException{
		StringBuilder sql = new StringBuilder()
		.append("SELECT AVG(AFS.UP_BYTES/1000000) AS UP, AVG(AFS.DOWN_BYTES/1000000) AS DOWN ")
		.append(" FROM elog.dbo.VW_APP_FLOW_STAT AFS")
		.append(" WHERE 'USG_").append(row.getgID())
		.append("' = AFS.DEVICENAME ")
		.append(" AND DATEADD(S,AFS.STATISTICTIME,'1970-01-01 00:00:00') ")
		.append(" BETWEEN DATEADD(HOUR, -24, GETUTCDATE()) ")
		.append(" AND GETUTCDATE()");
		//LOG.debug("SQL Statement [" + sql.toString() + "]");
		searchBySqlServer(row, sql.toString());
	}
	


	/**
	 * Global Method to search records by one or more parameters.
	 *
	 * @param sql
	 *            the sql
	 * @param params
	 *            the params
	 * @return List
	 * @throws SQLOperationException
	 *             the SQL operation exception
	 */
	private List<DashboardRow> searchBy(String sql) throws SQLOperationException {
		java.sql.Connection connection = null;
		java.sql.PreparedStatement pst = null;
		java.sql.ResultSet rs = null;
		List<DashboardRow> listOfRecords = null;
		
		try {
			connection = locator.getConnection();
			pst = connection.prepareStatement(sql);
			rs = pst.executeQuery();
			listOfRecords = buildList(rs);
		} catch (SQLException e) {
			throw new SQLOperationException(e);
		} finally {
			try {
				if (rs != null) rs.close();
				if (pst != null) pst.close();
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				throw new SQLOperationException(e);
			}
		}
		return listOfRecords;
	}
	
	private void searchBySqlServer(DashboardRow row, String sql) throws SQLOperationException {
		
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		try {
			
			pst = connectionSqlServer.prepareStatement(sql);
			rs = pst.executeQuery();
			while (rs.next()) {
				row.setAnchoBandaSubida(rs.getString("UP"));
				row.setAnchoBandaBajada(rs.getString("DOWN"));
			}
		} catch (SQLException e) {
			throw new SQLOperationException(e);
		} finally {
			try {
				if (rs != null) rs.close();
				if (pst != null) pst.close();
			} catch (SQLException e) {
				throw new SQLOperationException(e);
			}
		}
	}

	private  void searchByAxtel(DashboardRow row, String sql) throws SQLOperationException {
		PreparedStatement pst = null;
		ResultSet rs = null;
		StringBuilder paso;
		try {
			
			pst = connectionAxtel.prepareStatement(sql);
			rs = pst.executeQuery();
			while (rs.next()) {
				paso = new StringBuilder().append(rs.getNString("NESTATE"));
				switch (paso.toString().trim()) { 
				case "0": row.setEstadoDelServicio("No detectado");
					break;
				case "1": row.setEstadoDelServicio("En linea");
					break;
				case "2": row.setEstadoDelServicio("Fuera de linea");
					break;
				default:  row.setEstadoDelServicio("Invalido");
				}
				row.setTrafico(rs.getNString("SPEED"));
				row.setLatencia(rs.getNString("NEPIN"));
			}
		} catch (SQLException e) {
			throw new SQLOperationException(e);
		} finally {
			try {
				if (rs != null) rs.close();
				if (pst != null) pst.close();
			} catch (SQLException e) {
				throw new SQLOperationException(e);
			}
		}
		
	}
		

	/**
	 * create a List of records from Main tables
	 *
	 * @param rs
	 *            the rs
	 * @return List
	 * @throws SQLException
	 *             the SQL exception
	 */
	private List<DashboardRow> buildList(java.sql.ResultSet rs)
			throws SQLException {
		DashboardRow record = null;
		List<DashboardRow> listOfRecords = new ArrayList<DashboardRow>();
		while (rs.next()) {
			record = new DashboardRow();
			
			record.setgID(rs.getNString("GID"));
			record.setFechaInstSitioAxtel(rs.getNString("FECHA"));
			record.setFechaFirmaActaInicioOper(rs.getNString("FECHA_FIRMA_ACTA_INICIO_OPER"));
			record.setIpHomologada(rs.getNString("DIRECCION_IP_HOMOLOGADA"));
			record.setEntidadFederativa(rs.getNString("NOMBRE_ESTADO"));
			record.setMunicipio(rs.getNString("MUNICIPIO"));
			record.setLocalidad(rs.getNString("LOCALIDAD"));
			record.setInstitucion(rs.getNString("INSTITUCION"));
			record.setTasaTransferenciaBajada(rs.getNString("SUBIDA"));
			record.setTasaTransferenciaSubida(rs.getNString("BAJADA"));
			listOfRecords.add(record);
		}
		return listOfRecords;
	}
	

}
