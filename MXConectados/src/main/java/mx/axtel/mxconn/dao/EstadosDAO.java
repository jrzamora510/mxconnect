package mx.axtel.mxconn.dao;

import mx.axtel.mxconn.beans.Estado;
import mx.axtel.mxconn.exceptions.SQLOperationException;
import mx.axtel.mxconn.utils.ServiceLocator;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class EstadosDAO {

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(EstadosDAO.class);
	
	@Autowired
	private ServiceLocator locator;

	/**
	 * Instantiates a new Estados.
	 */
	public EstadosDAO() {
		LOG.debug("Retrieve states");
	}

	/**
	 * Get all states from CAT_ESTADOS Table.
	 *
	 * @return the java.util. list
	 * @throws SQLOperationException
	 *             the SQL operation exception
	 */
	public java.util.List<Estado> obtieneListaEstados()
			throws SQLOperationException {
		java.sql.Connection connection = null;
		java.sql.PreparedStatement pst = null;
		java.sql.ResultSet rs = null;
		Estado estado = null;
		java.util.List<Estado> listOfRecords = null;
		String sql = "SELECT CVE_ESTADO, NOMBRE_ESTADO FROM cat_estados WHERE IND_ACTIVO = 1 ORDER BY NOMBRE_ESTADO";
		try {
			connection = locator.getConnection();
			pst = connection.prepareStatement(sql.toString());
			rs = pst.executeQuery();
			listOfRecords = new java.util.ArrayList<Estado>();
			while (rs.next()) {
				estado = new Estado();
				estado.setId(rs.getInt(1));
				estado.setNombre(rs.getNString(2));
				listOfRecords.add(estado);
			}
		} catch (java.sql.SQLException e) {
			throw new SQLOperationException(e);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pst != null)
					pst.close();
				if (connection != null)
					connection.close();
			} catch (java.sql.SQLException e) {
				// TODO Auto-generated catch block
				throw new SQLOperationException(e);
			}
		}
		return listOfRecords;
	}

	/**
	 * Search by index colum.
	 *
	 * @param idx
	 *            the idx
	 * @return the java.util. list
	 * @throws SQLOperationException
	 *             the SQL operation exception
	 */
	public Estado buscaEstado(int idx)
			throws SQLOperationException {
		return null;
	}

}
