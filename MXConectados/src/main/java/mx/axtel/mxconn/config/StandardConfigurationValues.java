package mx.axtel.mxconn.config;

/**
 * The Class StandardConfigurationValues.
 */
public final class StandardConfigurationValues {
	
	/** The instance. */
	private static StandardConfigurationValues instance = null;
	
	/** The default state key. */
	private String defaultStateKey = null;
	
	/**
	 * Gets the default state key.
	 * @return the defaultStateKey
	 */
	public String getDefaultStateKey() {
		return defaultStateKey;
	}

	/**
	 * Sets the default state key.
	 * @param defaultStateKey the defaultStateKey to set
	 */
	public void setDefaultStateKey(String defaultStateKey) {
		this.defaultStateKey = defaultStateKey;
	}

	/**
	 * Instantiates a new standard configuration values.
	 */
	private StandardConfigurationValues() { }
	
	/**
	 * Gets the single instance of StandardConfigurationValues.
	 * @return single instance of StandardConfigurationValues
	 */
	public static StandardConfigurationValues getInstance() {
		if (instance == null) {
			synchronized (StandardConfigurationValues.class) {
				instance = new StandardConfigurationValues();
			}
		}
		return instance;
	}
}
