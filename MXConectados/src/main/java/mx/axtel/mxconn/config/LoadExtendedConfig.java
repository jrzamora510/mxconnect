package mx.axtel.mxconn.config;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

import org.apache.log4j.Logger;

/**
 * Servlet implementation class LoadExtendedConfig.
 */
@WebServlet(description = "Load Configuration params")
public class LoadExtendedConfig extends HttpServlet {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(LoadExtendedConfig.class);
       
	/**
	 * Init.
	 *
	 * @param config the config
	 * @throws ServletException the servlet exception
	 * @see Servlet#init(ServletConfig)
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		String defaultState = config.getInitParameter("DEF_STATE_KEY");
		StandardConfigurationValues conf = StandardConfigurationValues.getInstance();
		conf.setDefaultStateKey(defaultState);
		LOG.debug("DEF_STATE_KEY [" + conf.getDefaultStateKey() + "]");
	}
}
