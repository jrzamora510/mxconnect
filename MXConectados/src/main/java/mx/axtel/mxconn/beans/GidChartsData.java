package mx.axtel.mxconn.beans;

import java.util.List;

/**
 * The Class DashboardData.
 */
public class GidChartsData {
	
	/** The codigo. */
	private transient String codigo = "OK";
	
	/**  Datos para graficos. */
	private List<BandWidthBean> anchoDeBanda = null;
	
	/** The tasa de transferencia. */
	private List<TransferRateBean> tasaDeTransferencia = null;
	
	/** The latencia. */
	private List<LatencyBean> latencia = null;
	
	/** The protocolos. */
	private List<ProtocolBean> protocolos = null;
	
	/**
	 * Gets the codigo.
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * Sets the codigo.
	 * @param codigo the new codigo
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	/**
	 * Gets the ancho de banda.
	 *
	 * @return the anchoDeBanda
	 */
	public List<BandWidthBean> getAnchoDeBanda() {
		return anchoDeBanda;
	}

	/**
	 * Sets the ancho de banda.
	 *
	 * @param anchoDeBanda the anchoDeBanda to set
	 */
	public void setAnchoDeBanda(List<BandWidthBean> anchoDeBanda) {
		this.anchoDeBanda = anchoDeBanda;
	}

	/**
	 * Gets the tasa de transferencia.
	 *
	 * @return the tasaDeTransferencia
	 */
	public List<TransferRateBean> getTasaDeTransferencia() {
		return tasaDeTransferencia;
	}

	/**
	 * Sets the tasa de transferencia.
	 *
	 * @param tasaDeTransferencia the tasaDeTransferencia to set
	 */
	public void setTasaDeTransferencia(List<TransferRateBean> tasaDeTransferencia) {
		this.tasaDeTransferencia = tasaDeTransferencia;
	}

	/**
	 * Gets the latencia.
	 *
	 * @return the latencia
	 */
	public List<LatencyBean> getLatencia() {
		return latencia;
	}

	/**
	 * Sets the latencia.
	 *
	 * @param latencia the latencia to set
	 */
	public void setLatencia(List<LatencyBean> latencia) {
		this.latencia = latencia;
	}

	public List<ProtocolBean> getProtocolos() {
		return protocolos;
	}

	public void setProtocolos(List<ProtocolBean> protocolos) {
		this.protocolos = protocolos;
	}
	
}
