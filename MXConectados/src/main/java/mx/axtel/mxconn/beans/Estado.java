package mx.axtel.mxconn.beans;

import java.io.Serializable;

/**
 * The Class Estado.
 */
public class Estado implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -77229697683245819L;

	/** The id. */
	private Integer id;
	
	/** The nombre. */
	private String nombre;

	/**
	 * Gets the id.
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	
	/**
	 * Sets the id.
	 * @param id the new id
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	/**
	 * Gets the nombre.
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	
	/**
	 * Sets the nombre.
	 * @param nombre the new nombre
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}
