/**
 * 
 */
package mx.axtel.mxconn.beans;

/**
 * @author Luigi
 *
 */
public class TransferRateBean {
	
	private String deviceName = null;
	
	private String neState = null;
	
	private String timeStamp = null;
	
	private String time = null;
	
	private String displayValue = null;
	
	private String transferRateOut = null;
	
	private String transferRateIn = null;
	
	private String gid = null;

	/**
	 * @return the deviceName
	 */
	public String getDeviceName() {
		return deviceName;
	}

	/**
	 * @param deviceName the deviceName to set
	 */
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	/**
	 * @return the neState
	 */
	public String getNeState() {
		return neState;
	}

	/**
	 * @param neState the neState to set
	 */
	public void setNeState(String neState) {
		this.neState = neState;
	}

	/**
	 * @return the timeStamp
	 */
	public String getTimeStamp() {
		return timeStamp;
	}

	/**
	 * @param timeStamp the timeStamp to set
	 */
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	/**
	 * @return the time
	 */
	public String getTime() {
		return time;
	}

	/**
	 * @param time the time to set
	 */
	public void setTime(String time) {
		this.time = time;
	}

	/**
	 * @return the displayValue
	 */
	public String getDisplayValue() {
		return displayValue;
	}

	/**
	 * @param displayValue the displayValue to set
	 */
	public void setDisplayValue(String displayValue) {
		this.displayValue = displayValue;
	}

	/**
	 * @return the transferRateOut
	 */
	public String getTransferRateOut() {
		return transferRateOut;
	}

	/**
	 * @param transferRateOut the transferRateOut to set
	 */
	public void setTransferRateOut(String transferRateOut) {
		this.transferRateOut = transferRateOut;
	}

	/**
	 * @return the transferRateIn
	 */
	public String getTransferRateIn() {
		return transferRateIn;
	}

	/**
	 * @param transferRateIn the transferRateIn to set
	 */
	public void setTransferRateIn(String transferRateIn) {
		this.transferRateIn = transferRateIn;
	}

	/**
	 * @return the gid
	 */
	public String getGid() {
		return gid;
	}

	/**
	 * @param gid the gid to set
	 */
	public void setGid(String gid) {
		this.gid = gid;
	}

}
