package mx.axtel.mxconn.beans;

import java.io.Serializable;

/**
 * The Class ResultadoOperacion.
 */
public class ResultadoOperacion implements Serializable {
	
	/** The Constant OK. */
	private static final String OK_CODE = "OK";

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7560698607673048893L;

	/** The codigo. */
	private String codigo;
	
	/** The mensaje. */
	private String mensaje;
	
	/**
	 * Instantiates a new resultado operacion.
	 */
	public ResultadoOperacion() {
		codigo = OK_CODE;
		mensaje = "";
	}

	/**
	 * Gets the codigo.
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * Sets the codigo.
	 * @param codigo the new codigo
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/**
	 * Gets the mensaje.
	 * @return the mensaje
	 */
	public String getMensaje() {
		return mensaje;
	}

	/**
	 * Sets the mensaje.
	 * @param mensaje the new mensaje
	 */
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
}
