package mx.axtel.mxconn.beans;

import java.io.Serializable;

/**
 * The Class Usuario.
 */
public class Usuario implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3063055136156745744L;

	/** The user. */
	private String user;
	
	/** The password. */
	private String password;
	
	/** The tipo usuario. */
	private String tipoUsuario;
	
	/** The correo electronico. */
	private String correoElectronico;
	
	/** The nuevo password. */
	private String nuevoPassword;
	
	private int bloqueado;

	public int getBloqueado() {
		return bloqueado;
	}

	public void setBloqueado(int bloqueado) {
		this.bloqueado = bloqueado;
	}

	/**
	 * Gets the user.
	 * @return the user
	 */
	public String getUser() {
		return user;
	}

	/**
	 * Sets the user.
	 * @param user the new user
	 */
	public void setUser(String user) {
		this.user = user;
	}

	/**
	 * Gets the password.
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the password.
	 * @param password the new password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Gets the tipo usuario.
	 * @return the tipo usuario
	 */
	public String getTipoUsuario() {
		return tipoUsuario;
	}

	/**
	 * Sets the tipo usuario.
	 * @param tipoUsuario the new tipo usuario
	 */
	public void setTipoUsuario(String tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	/**
	 * Gets the correo electronico.
	 * @return the correo electronico
	 */
	public String getCorreoElectronico() {
		return correoElectronico;
	}

	/**
	 * Sets the correo electronico.
	 * @param correoElectronico the new correo electronico
	 */
	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	/**
	 * Gets the nuevo password.
	 * @return the nuevo password
	 */
	public String getNuevoPassword() {
		return nuevoPassword;
	}

	/**
	 * Sets the nuevo password.
	 * @param nuevoPassword the new nuevo password
	 */
	public void setNuevoPassword(String nuevoPassword) {
		this.nuevoPassword = nuevoPassword;
	}
}
