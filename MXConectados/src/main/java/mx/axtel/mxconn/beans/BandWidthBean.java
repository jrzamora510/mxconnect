/**
 * 
 */
package mx.axtel.mxconn.beans;

/**
 * @author Luigi
 *
 */
public class BandWidthBean {
	
	private String deviceName = null;
	
	private String neState = null;
	
	private String StatisticTime = null;
	
	private String time = null;
	
	private String upBytes = null;
	
	private String downBytes = null;
	
	private String gid = null;

	/**
	 * @return the deviceName
	 */
	public String getDeviceName() {
		return deviceName;
	}

	/**
	 * @param deviceName the deviceName to set
	 */
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	/**
	 * @return the neState
	 */
	public String getNeState() {
		return neState;
	}

	/**
	 * @param neState the neState to set
	 */
	public void setNeState(String neState) {
		this.neState = neState;
	}

	/**
	 * @return the statisticTime
	 */
	public String getStatisticTime() {
		return StatisticTime;
	}

	/**
	 * @param statisticTime the statisticTime to set
	 */
	public void setStatisticTime(String statisticTime) {
		StatisticTime = statisticTime;
	}

	/**
	 * @return the time
	 */
	public String getTime() {
		return time;
	}

	/**
	 * @param time the time to set
	 */
	public void setTime(String time) {
		this.time = time;
	}

	/**
	 * @return the upBytes
	 */
	public String getUpBytes() {
		return upBytes;
	}

	/**
	 * @param upBytes the upBytes to set
	 */
	public void setUpBytes(String upBytes) {
		this.upBytes = upBytes;
	}

	/**
	 * @return the downBytes
	 */
	public String getDownBytes() {
		return downBytes;
	}

	/**
	 * @param downBytes the downBytes to set
	 */
	public void setDownBytes(String downBytes) {
		this.downBytes = downBytes;
	}

	/**
	 * @return the gid
	 */
	public String getGid() {
		return gid;
	}

	/**
	 * @param gid the gid to set
	 */
	public void setGid(String gid) {
		this.gid = gid;
	}

}
