package mx.axtel.mxconn.beans;

import java.io.Serializable;

public class FiltroConsultaDashboard implements Serializable {

	private static final long serialVersionUID = 1801608419648754077L;
	private String gid;
	private String estado;

	public String getGid() {
		return gid;
	}

	public void setGid(String gid) {
		this.gid = gid;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
}
