package mx.axtel.mxconn.beans;

public class ProtocolBean {
	private String deviceName = null;
	
	private String neState = null;
	
	private String timeStamp = null;
	
	private String time = null;
	
	private String appId = null;
	
	private String appName = null;

	private String appSubtypeId = null;
	
	private String appSubtimeName = null;
	
	private String gid = null;
	
	private String protocol = null;
	
	private String total = null;
	
	private String percent = null;

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getNeState() {
		return neState;
	}

	public void setNeState(String neState) {
		this.neState = neState;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getAppSubtypeId() {
		return appSubtypeId;
	}

	public void setAppSubtypeId(String appSubtypeId) {
		this.appSubtypeId = appSubtypeId;
	}

	public String getAppSubtimeName() {
		return appSubtimeName;
	}

	public void setAppSubtimeName(String appSubtimeName) {
		this.appSubtimeName = appSubtimeName;
	}

	public String getGid() {
		return gid;
	}

	public void setGid(String gid) {
		this.gid = gid;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getPercent() {
		return percent;
	}

	public void setPercent(String percent) {
		this.percent = percent;
	}
	
}
