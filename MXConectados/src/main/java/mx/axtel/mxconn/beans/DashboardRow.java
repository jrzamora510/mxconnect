package mx.axtel.mxconn.beans;

/**
 * The Class DashboardRow.
 */
public class DashboardRow {
	
	/** The gid. */
	private String gID = "<empty>";
	
	/** The fecha inst sitio axtel. */
	private String fechaInstSitioAxtel = "<empty>";
	
	/** The fecha firma acta inicio oper. */
	private String fechaFirmaActaInicioOper = "<empty>";
	
	/** The ancho banda subida. */
	private String anchoBandaSubida = "<empty>";
	
	/** The ancho banda bajada. */
	private String anchoBandaBajada = "<empty>";
	
	/** The ip homologada. */
	private String ipHomologada = "<empty>";
	
	/** The estado del servicio. */
	private String estadoDelServicio = "<empty>";
	
	/** The trafico. */
	private String trafico = "<empty>";
	
	/** The entidad federativa. */
	private String entidadFederativa = "<empty>";
	
	/** The municipio. */
	private String municipio = "<empty>";
	
	/** The localidad. */
	private String localidad = "<empty>";
	
	/** The institucion. */
	private String institucion = "<empty>";
	
	/** The tasa transferencia subida. */
	private String tasaTransferenciaSubida = "<empty>";
	
	/** The tasa transferencia bajada. */
	private String tasaTransferenciaBajada = "<empty>";
	
	/** The latencia. */
	private String latencia = "<empty>";
	
	
	/*
	 * additional fields
	 */
	/** The role. */
	private String role = "<empty>";
	
	/** The userid. */
	private String userid = "<empty>";
	
	/** The time stamp. */
	private String timeStamp = "<empty>";
	
	/** The field1. */
	private String field1 = "<empty>";
	
	/** The field2. */
	private String field2 = "<empty>";
	
	/** The field3. */
	private String field3 = "<empty>";
	
	/** The field4. */
	private String field4 = "<empty>";

	/**
	 * Gets the g id.
	 *
	 * @return the gID
	 */
	public String getgID() {
		return gID;
	}

	/**
	 * Sets the g id.
	 *
	 * @param gID the gID to set
	 */
	public void setgID(String gID) {
		this.gID = gID;
	}

	/**
	 * Gets the fecha inst sitio axtel.
	 *
	 * @return the fechaInstSitioAxtel
	 */
	public String getFechaInstSitioAxtel() {
		return fechaInstSitioAxtel;
	}

	/**
	 * Sets the fecha inst sitio axtel.
	 *
	 * @param fechaInstSitioAxtel the fechaInstSitioAxtel to set
	 */
	public void setFechaInstSitioAxtel(String fechaInstSitioAxtel) {
		this.fechaInstSitioAxtel = fechaInstSitioAxtel;
	}

	/**
	 * Gets the fecha firma acta inicio oper.
	 *
	 * @return the fechaFirmaActaInicioOper
	 */
	public String getFechaFirmaActaInicioOper() {
		return fechaFirmaActaInicioOper;
	}

	/**
	 * Sets the fecha firma acta inicio oper.
	 *
	 * @param fechaFirmaActaInicioOper the fechaFirmaActaInicioOper to set
	 */
	public void setFechaFirmaActaInicioOper(String fechaFirmaActaInicioOper) {
		this.fechaFirmaActaInicioOper = fechaFirmaActaInicioOper;
	}

	/**
	 * Gets the ancho banda subida.
	 *
	 * @return the anchoBandaSubida
	 */
	public String getAnchoBandaSubida() {
		return anchoBandaSubida;
	}

	/**
	 * Sets the ancho banda subida.
	 *
	 * @param anchoBandaSubida the anchoBandaSubida to set
	 */
	public void setAnchoBandaSubida(String anchoBandaSubida) {
		this.anchoBandaSubida = anchoBandaSubida;
	}

	/**
	 * Gets the ancho banda bajada.
	 *
	 * @return the anchoBandaBajada
	 */
	public String getAnchoBandaBajada() {
		return anchoBandaBajada;
	}

	/**
	 * Sets the ancho banda bajada.
	 *
	 * @param anchoBandaBajada the anchoBandaBajada to set
	 */
	public void setAnchoBandaBajada(String anchoBandaBajada) {
		this.anchoBandaBajada = anchoBandaBajada;
	}

	/**
	 * Gets the ip homologada.
	 *
	 * @return the ipHomologada
	 */
	public String getIpHomologada() {
		return ipHomologada;
	}

	/**
	 * Sets the ip homologada.
	 *
	 * @param ipHomologada the ipHomologada to set
	 */
	public void setIpHomologada(String ipHomologada) {
		this.ipHomologada = ipHomologada;
	}

	/**
	 * Gets the estado del servicio.
	 *
	 * @return the estadoDelServicio
	 */
	public String getEstadoDelServicio() {
		return estadoDelServicio;
	}

	/**
	 * Sets the estado del servicio.
	 *
	 * @param estadoDelServicio the estadoDelServicio to set
	 */
	public void setEstadoDelServicio(String estadoDelServicio) {
		this.estadoDelServicio = estadoDelServicio;
	}

	/**
	 * Gets the trafico.
	 *
	 * @return the trafico
	 */
	public String getTrafico() {
		return trafico;
	}

	/**
	 * Sets the trafico.
	 *
	 * @param trafico the trafico to set
	 */
	public void setTrafico(String trafico) {
		this.trafico = trafico;
	}

	/**
	 * Gets the entidad federativa.
	 *
	 * @return the entidadFederativa
	 */
	public String getEntidadFederativa() {
		return entidadFederativa;
	}

	/**
	 * Sets the entidad federativa.
	 *
	 * @param entidadFederativa the entidadFederativa to set
	 */
	public void setEntidadFederativa(String entidadFederativa) {
		this.entidadFederativa = entidadFederativa;
	}

	/**
	 * Gets the municipio.
	 *
	 * @return the municipio
	 */
	public String getMunicipio() {
		return municipio;
	}

	/**
	 * Sets the municipio.
	 *
	 * @param municipio the municipio to set
	 */
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	/**
	 * Gets the localidad.
	 *
	 * @return the localidad
	 */
	public String getLocalidad() {
		return localidad;
	}

	/**
	 * Sets the localidad.
	 *
	 * @param localidad the localidad to set
	 */
	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	/**
	 * Gets the institucion.
	 *
	 * @return the institucion
	 */
	public String getInstitucion() {
		return institucion;
	}

	/**
	 * Sets the institucion.
	 *
	 * @param institucion the institucion to set
	 */
	public void setInstitucion(String institucion) {
		this.institucion = institucion;
	}

	/**
	 * Gets the tasa transferencia subida.
	 *
	 * @return the tasaTransferenciaSubida
	 */
	public String getTasaTransferenciaSubida() {
		return tasaTransferenciaSubida;
	}

	/**
	 * Sets the tasa transferencia subida.
	 *
	 * @param tasaTransferenciaSubida the tasaTransferenciaSubida to set
	 */
	public void setTasaTransferenciaSubida(String tasaTransferenciaSubida) {
		this.tasaTransferenciaSubida = tasaTransferenciaSubida;
	}

	/**
	 * Gets the tasa transferencia bajada.
	 *
	 * @return the tasaTransferenciaBajada
	 */
	public String getTasaTransferenciaBajada() {
		return tasaTransferenciaBajada;
	}

	/**
	 * Sets the tasa transferencia bajada.
	 *
	 * @param tasaTransferenciaBajada the tasaTransferenciaBajada to set
	 */
	public void setTasaTransferenciaBajada(String tasaTransferenciaBajada) {
		this.tasaTransferenciaBajada = tasaTransferenciaBajada;
	}

	/**
	 * Gets the latencia.
	 *
	 * @return the latencia
	 */
	public String getLatencia() {
		return latencia;
	}

	/**
	 * Sets the latencia.
	 *
	 * @param latencia the latencia to set
	 */
	public void setLatencia(String latencia) {
		this.latencia = latencia;
	}

	/**
	 * Gets the role.
	 *
	 * @return the role
	 */
	public String getRole() {
		return role;
	}

	/**
	 * Sets the role.
	 *
	 * @param role the role to set
	 */
	public void setRole(String role) {
		this.role = role;
	}

	/**
	 * Gets the userid.
	 *
	 * @return the userid
	 */
	public String getUserid() {
		return userid;
	}

	/**
	 * Sets the userid.
	 *
	 * @param userid the userid to set
	 */
	public void setUserid(String userid) {
		this.userid = userid;
	}

	/**
	 * Gets the time stamp.
	 *
	 * @return the timeStamp
	 */
	public String getTimeStamp() {
		return timeStamp;
	}

	/**
	 * Sets the time stamp.
	 *
	 * @param timeStamp the timeStamp to set
	 */
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	/**
	 * Gets the field1.
	 *
	 * @return the field1
	 */
	public String getField1() {
		return field1;
	}

	/**
	 * Sets the field1.
	 *
	 * @param field1 the field1 to set
	 */
	public void setField1(String field1) {
		this.field1 = field1;
	}

	/**
	 * Gets the field2.
	 *
	 * @return the field2
	 */
	public String getField2() {
		return field2;
	}

	/**
	 * Sets the field2.
	 *
	 * @param field2 the field2 to set
	 */
	public void setField2(String field2) {
		this.field2 = field2;
	}

	/**
	 * Gets the field3.
	 *
	 * @return the field3
	 */
	public String getField3() {
		return field3;
	}

	/**
	 * Sets the field3.
	 *
	 * @param field3 the field3 to set
	 */
	public void setField3(String field3) {
		this.field3 = field3;
	}

	/**
	 * Gets the field4.
	 *
	 * @return the field4
	 */
	public String getField4() {
		return field4;
	}

	/**
	 * Sets the field4.
	 *
	 * @param field4 the field4 to set
	 */
	public void setField4(String field4) {
		this.field4 = field4;
	}

}
