package mx.axtel.mxconn.beans;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class DashboardData.
 */
public class DashboardData {
	
	/** The rows. */
	private transient final List<DashboardRow> rows;

	/** The pagina actual. */
	private transient Integer paginaActual = 0;
	
	/** The numero paginas. */
	private transient Integer numeroPaginas = 0;
	
	/** The codigo. */
	private transient String codigo = "OK";
	
	/**  Catalogo Estados. */
	private List<Estado> listaEstados = null;
	
	/**
	 * Instantiates a new dashboard data.
	 */
	public DashboardData() {
		rows = new ArrayList<DashboardRow>();
	}
	
	/**
	 * Instantiates a new dashboard data.
	 * @param rowsP the rows p
	 */
	public DashboardData(List<DashboardRow> rowsP) {
		if(rowsP != null) {
			rows = rowsP;
		} else {
			rows = new ArrayList<DashboardRow>();
		}
	}

	/**
	 * Adds the row.
	 * @param row the row
	 */
	public void addRow(DashboardRow row) {
		rows.add(row);
	}
	
	/**
	 * Adds the all rows.
	 * @param rowsP the rows p
	 */
	public void addAllRows(List<DashboardRow> rowsP) {
		if(rowsP != null && rowsP.size() > 0) {
			rows.addAll(rowsP);
		}
	}
	
	/**
	 * Gets the rows.
	 * @return the rows
	 */
	public List<DashboardRow> getRows() {
		return rows;
	}

	/**
	 * Gets the pagina actual.
	 *
	 * @return the pagina actual
	 */
	public Integer getPaginaActual() {
		return paginaActual;
	}

	/**
	 * Sets the pagina actual.
	 * @param paginaActual the new pagina actual
	 */
	public void setPaginaActual(Integer paginaActual) {
		this.paginaActual = paginaActual;
	}

	/**
	 * Gets the numero paginas.
	 * @return the numero paginas
	 */
	public Integer getNumeroPaginas() {
		return numeroPaginas;
	}

	/**
	 * Sets the numero paginas.
	 * @param numeroPaginas the new numero paginas
	 */
	public void setNumeroPaginas(Integer numeroPaginas) {
		this.numeroPaginas = numeroPaginas;
	}

	/**
	 * Gets the codigo.
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * Sets the codigo.
	 * @param codigo the new codigo
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	/**
	 * Gets the lista estados.
	 *
	 * @return the lista estados
	 */
	public List<Estado> getListaEstados() {
		return listaEstados;
	}

	/**
	 * Sets the lista estados.
	 *
	 * @param listaEstados the new lista estados
	 */
	public void setListaEstados(List<Estado> listaEstados) {
		this.listaEstados = listaEstados;
	}

}
