package mx.axtel.mxconn.controllers;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * The Class ReportesController.
 */
@Controller
public class ReportesController {
	
	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(ReportesController.class);
	
	/**
	 * Reporte amenazas.
	 *
	 * @return the model and view
	 */
	@RequestMapping(value = "/reporteAmenazas.do")
	public ModelAndView reporteAmenazas() {
		LOG.debug("Cargando vista de reporte amenazas...");
		return new ModelAndView("reporte-amenazas");
	}
}
