package mx.axtel.mxconn.controllers;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * The Class GlobalController.
 */
@Controller
public class GlobalController {
	
	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(GlobalController.class);
	
	/**
	 * Login.
	 * @return the model and view
	 */
	@RequestMapping(value = "/login.go")
	public ModelAndView login() {
		LOG.debug("Cargando vista login...");
		return new ModelAndView("login-page");
	}
	
	/**
	 * Error.
	 * @return the model and view
	 */
	@RequestMapping(value = "/errorPage.do")
	public ModelAndView error() {
		LOG.debug("Error!");
		return new ModelAndView("error-page");
	}
	
	/**
	 * Help.
	 * @return the model and view
	 */
	@RequestMapping(value = "/help.do")
	public ModelAndView help() {
		LOG.debug("Error!");
		return new ModelAndView("help-page");
	}
}
