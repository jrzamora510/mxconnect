package mx.axtel.mxconn.controllers;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.axtel.mxconn.beans.BandWidthBean;
import mx.axtel.mxconn.beans.DashboardData;
import mx.axtel.mxconn.beans.DashboardRow;
import mx.axtel.mxconn.beans.Estado;
import mx.axtel.mxconn.beans.FiltroConsultaDashboard;
import mx.axtel.mxconn.beans.LatencyBean;
import mx.axtel.mxconn.beans.ProtocolBean;
import mx.axtel.mxconn.beans.TransferRateBean;
import mx.axtel.mxconn.bo.MainDashboardBO;
import mx.axtel.mxconn.config.StandardConfigurationValues;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * The Class DashboardController.
 */
@Controller
public class DashboardController {
	
	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(DashboardController.class);

	/** The Constant MAIN_TABLE. */
	private static final String MAIN_TABLE = "tableRows";
	
	/** The Constant QUERY_STATE. */
	private static final String QUERY_STATE = "QueryState";

	/** The bo. */
	@Autowired
	private MainDashboardBO bo;
	
	/**
	 * Handle request.
	 * @param request the request
	 * @param response the response
	 * @return the model and view
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws SQLException 
	 */
	@RequestMapping(value = "/dashboard.do")
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
		LOG.info("Generating index view...");
		
		StandardConfigurationValues conf = StandardConfigurationValues.getInstance();
		List<DashboardRow> listOfRecords = bo.buildMainTableWithStateName(conf.getDefaultStateKey());
		
		Map<String, Object> model = new HashMap<String, Object>();
		model.put(MAIN_TABLE, listOfRecords);
		model.put(QUERY_STATE, conf.getDefaultStateKey());
		
		LOG.info("Returning index view");
		return new ModelAndView("index-page", model);
	}
	
	/**
	 * Consulta dashboard.
	 * @param filtro the filtro
	 * @return the dashboard data
	 * @throws SQLException 
	 */
	@RequestMapping(value = "/consultaDashboard.do", method = RequestMethod.POST)
	public @ResponseBody DashboardData consultaDashboard(@RequestBody final FiltroConsultaDashboard filtro) throws SQLException {
		String estado = filtro.getEstado();
		String gid = filtro.getGid();
		
		LOG.debug("filtro:");
		LOG.debug(gid);
		LOG.debug(estado);
		
		List<DashboardRow> listOfRecords = null;
		StandardConfigurationValues conf = StandardConfigurationValues.getInstance();
		
		if(!StringUtils.isEmpty(estado)) {
			listOfRecords = bo.buildMainTableWithStateName(estado.toUpperCase());
		} else if(!StringUtils.isEmpty(gid)) {
			listOfRecords = bo.buildMainTableWithGID(gid);
		} else if(StringUtils.isEmpty(estado)) {
			listOfRecords = bo.buildMainTableWithStateName(conf.getDefaultStateKey());
		}
		return new DashboardData(listOfRecords);
	}
	
	@RequestMapping(value = "/consultaEstados.do", method = RequestMethod.POST)
	public @ResponseBody List<Estado> consultaEstados() {
		return bo.getCatEstados().getListaEstados();
	}
	
	@RequestMapping(value = "/consultaDatosAnchoBanda.do", method = RequestMethod.POST)
	public @ResponseBody List<BandWidthBean> consultaDatosAnchoBanda(@RequestBody final FiltroConsultaDashboard filtro) throws SQLException {
		String gid = filtro.getGid();
		return bo.getBandWidthInformation(gid).getAnchoDeBanda();
	}
	
	@RequestMapping(value = "/consultaDatosTasaTransferencia.do", method = RequestMethod.POST)
	public @ResponseBody List<TransferRateBean> consultaDatosTasaTransferencia(@RequestBody final FiltroConsultaDashboard filtro) throws SQLException {
		String gid = filtro.getGid();
		return bo.getTranferRateInformation(gid).getTasaDeTransferencia();
	}
	
	@RequestMapping(value = "/consultaDatosLatencia.do", method = RequestMethod.POST)
	public @ResponseBody List<LatencyBean> consultaDatosLatencia(@RequestBody final FiltroConsultaDashboard filtro) throws SQLException {
		String gid = filtro.getGid();
		return bo.getLatencyInformation(gid).getLatencia();
	}
	
	@RequestMapping(value = "/consultaDatosProtocolo.do", method = RequestMethod.POST)
	public @ResponseBody List<ProtocolBean> consultaDatosProtocolo(@RequestBody final FiltroConsultaDashboard filtro) throws SQLException {
		String gid = filtro.getGid();
		return bo.getProtocolInformation(gid).getProtocolos();
	}
	
		
}
