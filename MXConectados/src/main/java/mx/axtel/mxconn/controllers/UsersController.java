package mx.axtel.mxconn.controllers;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.axtel.mxconn.beans.DashboardRow;
import mx.axtel.mxconn.beans.ResultadoOperacion;
import mx.axtel.mxconn.beans.Usuario;
import mx.axtel.mxconn.bo.UsuariosBO;
import mx.axtel.mxconn.config.StandardConfigurationValues;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * The Class UsersController.
 */
@Controller
public class UsersController {
	
	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(UsersController.class);
	
	/** The Constant MAIN_TABLE. */
	private static final String MAIN_TABLE = "listUsuarios";
	
	/** The Constant QUERY_STATE. */
	private static final String QUERY_STATE = "QueryState";
	
	private static final String RESULTADO_OPERACION = "resultadoOperacion";
	
	private static final String ERROR_OPERACION = "errorOperacion";
	
	/** The usuarios bo. */
	@Autowired
	private UsuariosBO usuariosBO; 
	
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/eliminarUsuario.do")
	public @ResponseBody ModelAndView eliminarUsuario(HttpServletRequest request, HttpServletResponse response) throws SQLException, ServletException, IOException {
		LOG.debug("Eliminado usuario ....");
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    String idUsuario = request.getParameter("opcion");
		ResultadoOperacion resultado = usuariosBO.eliminarUsuario(idUsuario, auth.getName());
		
		return consultarUsuarios(resultado,request, response);
		
	}
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/bloquearUsuario.do")
	public @ResponseBody ModelAndView bloquearUsuario(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
		LOG.debug("bloqueando usuario ....");
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    String idUsuario = request.getParameter("opcion");
		ResultadoOperacion resultado = usuariosBO.bloquearUsuario(idUsuario, auth.getName(), 0);
		
		return consultarUsuarios(resultado, request, response);
	}
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/desbloquearUsuario.do")
	public @ResponseBody ModelAndView desbloquearUsuario(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
		LOG.debug("bloqueando usuario ....");
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    String idUsuario = request.getParameter("opcion");
		ResultadoOperacion resultado = usuariosBO.bloquearUsuario(idUsuario, auth.getName(), 1);
		
		return consultarUsuarios(resultado, request, response);
	}
	

	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/usuarios.do")
	public ModelAndView consultarUsuarios(ResultadoOperacion resultado, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
		LOG.debug("Cargando vista de usuarios...");
		
		StandardConfigurationValues conf = StandardConfigurationValues.getInstance();
		List<Usuario> listUsuarios = usuariosBO.consultarUsuarios();
		
		Map<String, Object> model = new HashMap<String, Object>();
		model.put(RESULTADO_OPERACION, resultado.getMensaje());
		model.put(ERROR_OPERACION, resultado.getCodigo());
		model.put(MAIN_TABLE, listUsuarios);
		model.put(QUERY_STATE, conf.getDefaultStateKey());
		
		return new ModelAndView("usuarios-page", model);
	}
	
	/**
	 * Register.
	 * @return the model and view
	 */
	@RequestMapping(value = "/register.do")
	public ModelAndView register() {
		LOG.debug("Cargando vista register...");
		return new ModelAndView("register-page");
	}
	
	/**
	 * Register.
	 * @return the model and view
	 */
	@RequestMapping(value = "/changePassword.do")
	public ModelAndView changePassword() {
		LOG.debug("Cargando vista changePassword...");
		return new ModelAndView("changePassword-page");
	}
	
	/**
	 * Agregar usuario.
	 * @param usr the usr
	 * @return the resultado operacion
	 */
	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/altaUsuario.do", method = RequestMethod.POST)
	public @ResponseBody ResultadoOperacion registrarUsuario(final @RequestBody Usuario usr) {
		LOG.debug("Agregando usuario al sistema");
		return usuariosBO.registrarUsuario(usr);
	}
	
	/**
	 * Cambio password.
	 * @param usr the usr
	 * @return the resultado operacion
	 */
	@RequestMapping(value = "/cambioPassword.do", method = RequestMethod.POST)
	public @ResponseBody ResultadoOperacion cambioPassword(final @RequestBody Usuario usr) {
		ResultadoOperacion resultadoOperacion = new ResultadoOperacion();
		LOG.debug("Ejecutando cambio de contraseņa");
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String userName = auth.getName();
		
		if(StringUtils.equals(userName, usr.getUser())) {
			resultadoOperacion = usuariosBO.cambioPassword(usr);
		} else {
			resultadoOperacion.setCodigo("ERR-USR00");
			resultadoOperacion.setMensaje("Operacion no permitida.");
		}
		LOG.debug("res: " + resultadoOperacion.getCodigo());
		return resultadoOperacion;
	}
}
