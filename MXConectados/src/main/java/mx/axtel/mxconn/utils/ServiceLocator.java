package mx.axtel.mxconn.utils;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * The Class ServiceLocator.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class ServiceLocator {

	/** The data source. */
	@Autowired
	private DataSource dataSource = null;
	@Autowired
	private DataSource dataSourceAxtel = null;
	@Autowired
	private DataSource dataSourceSqlServer = null;

	/**
	 * Gets the connection.
	 * @return the connection
	 * @throws SQLException the SQL exception
	 */
	public java.sql.Connection getConnection() throws SQLException {
		return dataSource.getConnection();
	}
	
	/**
	 * Gets the connection.
	 * @return the connection Axtel
	 * @throws SQLException the SQL exception
	 */
	public java.sql.Connection getConnectionAxtel() throws SQLException {
		return dataSourceAxtel.getConnection();
	}
	
	/**
	 * Gets the connection.
	 * @return the connection sqlServer
	 * @throws SQLException the SQL exception
	 */
	public java.sql.Connection getConnectionSqlServer() throws SQLException {
		return dataSourceSqlServer.getConnection();
	}
}
