package mx.axtel.mxconn.bo;

import java.sql.SQLException;
import java.util.List;

import mx.axtel.mxconn.beans.ResultadoOperacion;
import mx.axtel.mxconn.beans.Usuario;
import mx.axtel.mxconn.dao.UsuariosDAO;
import mx.axtel.mxconn.exceptions.SQLOperationException;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * The Class UsuariosBO.
 */
@Service
public class UsuariosBO {
	
	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(UsuariosBO.class);
	
	/** The usuarios dao. */
	@Autowired
	private UsuariosDAO usuariosDAO;
	
	public ResultadoOperacion eliminarUsuario(String idUser, String name) throws SQLException {
		ResultadoOperacion res = new ResultadoOperacion();
		int iResult = 0;
		res.setCodigo("0");
		if(idUser.equals(name)){
			res.setCodigo("ERR-USR14");
			res.setMensaje("No puedes eliminar tu propio usuario.");
		} else {
			try {
				iResult = usuariosDAO.eliminarUsuario(idUser);
				if(iResult == 1){
					res.setMensaje("El usuario ha sido eliminado de la base de datos del aplicativo.");
				} else {
					res.setCodigo("ERR-USR12");
					res.setMensaje("Ocurrio un problema al eliminar el usuario, consulte con soprte.");
				}
			} catch (SQLOperationException e) {
				LOG.error("Error en la base de datos", e);
			}
		}
		return res;
	}
	
	public ResultadoOperacion bloquearUsuario(String idUser, String name, int accion) {
		ResultadoOperacion res = new ResultadoOperacion();
		int iResul = 0;
		res.setCodigo("0");
		if(idUser.equals(name)){
			res.setCodigo("ERR-USR13");
			res.setMensaje("No puedes bloquear tu propio usuario.");
		} else {
			try {
				iResul = usuariosDAO.bloquearUsuario(idUser, accion);
				if(iResul == 1 && accion == 0){
					res.setMensaje("El usuario ha sido bloqueado del aplicativo.");
				} else if(iResul == 1 && accion == 1){
						res.setMensaje("El usuario ha sido desbloqueado del aplicativo.");
				} else {
					res.setCodigo("ERR-USR12");
					res.setMensaje("Ocurrio un problema, consulte con soprte.");
				}
			} catch (SQLOperationException e) {
				LOG.error("Error en la base de datos", e);
			}
		}
		return res;
	}
	
	public List<Usuario> consultarUsuarios() {
		List<Usuario> list = null;
		try {
			list = usuariosDAO.consultarUsuarios();
		} catch (SQLOperationException e) {
			LOG.error("Error en la base de datos", e);
		}
		return list;
	}

	/**
	 * Registrar usuario.
	 * @param usr the usr
	 * @return the resultado operacion
	 */
	public ResultadoOperacion registrarUsuario(Usuario usr) {
		LOG.debug("Ejecutando servicio de registro de usuario...");
		ResultadoOperacion res = new ResultadoOperacion();
		if(StringUtils.isBlank(usr.getUser())){
			res.setCodigo("ERR-USR01");
			res.setMensaje("Es necesario indicar el nombre de usuario.");
		} else if(StringUtils.isBlank(usr.getPassword())) {
			res.setCodigo("ERR-USR02");
			res.setMensaje("Es necesario especificar una contrase\u00F1a.");
		} else if(usr.getPassword().length() < 8) {
			res.setCodigo("ERR-USR03");
			res.setMensaje("La contrase\u00F1a debe ser de al menos 8 caracteres.");
		} else if(StringUtils.isBlank(usr.getTipoUsuario()) || (!usr.getTipoUsuario().equals("ROLE_USER") && !usr.getTipoUsuario().equals("ROLE_ADMIN"))) {
			res.setCodigo("ERR-USR04");
			res.setMensaje("Tipo de usuario incorrecto.");
		} else {
			try {
				PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
				usr.setPassword(passwordEncoder.encode(usr.getPassword()));
				usuariosDAO.insertarUsuario(usr);
				usr = null;
				res.setMensaje("Alta de usuario exitosa.");
			} catch (SQLOperationException e) {
				LOG.error("No es posible insertar el registro en BD", e);
				res.setCodigo("ERR-USR06");
				res.setMensaje("No es posible ejecutar la operaci\u00F3n, intente m\u00E1s tarde.");
			}
		}
		return res;
	}

	/**
	 * Cambio password.
	 * @param usr the usr
	 * @return the resultado operacion
	 */
	public ResultadoOperacion cambioPassword(Usuario usr) {
		ResultadoOperacion res = new ResultadoOperacion();
		try {
			String encodedPwd = usuariosDAO.consultaPasswordActual(usr.getUser());
			boolean checkpw = BCrypt.checkpw(usr.getPassword(), encodedPwd);
			if(checkpw) {
				if(usr.getNuevoPassword().length() < 8) {
					res.setCodigo("ERR-USR03");
					res.setMensaje("La contrase\u00F1a debe ser de al menos 8 caracteres.");
				} else {
					PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
					usr.setNuevoPassword(passwordEncoder.encode(usr.getNuevoPassword()));
					usuariosDAO.actualizaPassword(usr);
					res.setMensaje("El cambio de contrase\u00F1a se realiz\u00F3 correctamente.");
				}
			} else {
				res.setCodigo("ERR-USR07");
				res.setMensaje("Contrase\u00F1a incorrecta, verifique.");
			}
		} catch (SQLOperationException e) {
			LOG.error("No es posible insertar el registro en BD", e);
			res.setCodigo("ERR-USR08");
			res.setMensaje("No es posible ejecutar la operaci\u00F3n, intente m\u00E1s tarde.");
		}
		return res;
	}
}
