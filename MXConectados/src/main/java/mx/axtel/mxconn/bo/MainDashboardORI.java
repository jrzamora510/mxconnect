package mx.axtel.mxconn.bo;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import mx.axtel.mxconn.beans.DashboardData;
import mx.axtel.mxconn.beans.DashboardRow;
import mx.axtel.mxconn.beans.Estado;
import mx.axtel.mxconn.dao.EstadosDAO;
import mx.axtel.mxconn.dao.GIDOperations;
import mx.axtel.mxconn.entity.Gid;
import mx.axtel.mxconn.exceptions.SQLOperationException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * The Class MainDashboardBO.
 */
@Service
public class MainDashboardORI {
	
	/** The logger. */
	private static final Logger LOG = Logger.getLogger(MainDashboardORI.class);
	
	@Autowired
	private GIDOperations gid;
	
	@Autowired
	private EstadosDAO estadosDAO;
	
	/**
	 * Instantiates a new main dashboard bo.
	 */
	public MainDashboardORI() {
		System.out.println("Loading MainDashboardBO");
	}
	
	/**
	 * Build table using GID like Key.
	 *
	 * @param key the key
	 * @return the list
	 */
	public List<DashboardRow> buildMainTableWithGID(String key) {
		List<DashboardRow> listOfRecords = null;
		List<Gid> listOfGid = null;
		DashboardRow bean = null;

		try {
			listOfGid = gid.searchByPrimaryKey(key);
			if (listOfGid != null) {
				listOfRecords = new ArrayList<DashboardRow>();
				Iterator<Gid> iterator = listOfGid.iterator();
				while (iterator.hasNext()) {
					Gid record = iterator.next();
					if (record != null) {
						bean = new DashboardRow();
						bean.setgID(record.getGid());
						bean.setFechaInstSitioAxtel(record.getFecha());
						bean.setFechaFirmaActaInicioOper(record.getFechaFirmaActaInicioOper());
						bean.setIpHomologada(record.getDireccionIPHomologada());
						bean.setEntidadFederativa(record.getNombreDelEstado());
						bean.setInstitucion(record.getInstitucion());
						/*
						 * missing params...
						 */
						listOfRecords.add(bean);
					}
				}
			}
		} catch (SQLOperationException e) {
			listOfRecords = new ArrayList<DashboardRow>();
			bean = new DashboardRow();
			listOfRecords.add(bean);
			e.printStackTrace();
		}
		if (listOfRecords == null) {
			// return a single row
			listOfRecords = new ArrayList<DashboardRow>();
			bean = new DashboardRow();
			listOfRecords.add(bean);
			LOG.debug("CONSULTA A GID_PIVOTE X GID CON RESULTADOS NULOS");
		} else {
			LOG.debug("Records founds [" + listOfRecords.size() + "]");
		}
		return listOfRecords;
	}
	
	/**
	 * Build table using state like Key.
	 *
	 * @param state the state
	 * @return the list
	 */
	public List<DashboardRow> buildMainTableWithStateName(String state) {
		List<DashboardRow> listOfRecords = null;
		List<Gid> listOfGid = null;
		DashboardRow bean = null;
		try {
			listOfGid = gid.searchByIndexColum(state);
			if (listOfGid != null) {
				listOfRecords = new ArrayList<DashboardRow>();
				Iterator<Gid> iterator = listOfGid.iterator();
				while (iterator.hasNext()) {
					Gid record = iterator.next();
					if (record != null) {
						bean = new DashboardRow();
						bean.setgID(record.getGid());
						bean.setFechaInstSitioAxtel(record.getFecha());
						bean.setFechaFirmaActaInicioOper(record.getFechaFirmaActaInicioOper());
						bean.setIpHomologada(record.getDireccionIPHomologada());
						bean.setEntidadFederativa(record.getNombreDelEstado());
						bean.setInstitucion(record.getInstitucion());
						/*
						 * missing params...
						 */
						listOfRecords.add(bean);
					}
				}
			}
		} catch (SQLOperationException e) {
			// TODO Auto-generated catch block
			// return a single row
			listOfRecords = new ArrayList<DashboardRow>();
			bean = new DashboardRow();
			listOfRecords.add(bean);
			e.printStackTrace();
		}
		if (listOfRecords == null) {
			// return a single row
			listOfRecords = new ArrayList<DashboardRow>();
			bean = new DashboardRow();
			listOfRecords.add(bean);
			LOG.debug("CONSULTA A GID_PIVOTE x ESTADO CON RESULTADOS NULOS");
		} else {
			LOG.debug("Records founds [" + listOfRecords.size() + "]");
		}
		return listOfRecords;
	}
	
	/**
	 * Build table using state like Key.
	 *
	 * @param state the state
	 * @return the list
	 */
	public DashboardData getCatEstados() {
		java.util.List<Estado> listOfRecords = null;
		DashboardData data = new DashboardData();
		try {
			listOfRecords = estadosDAO.obtieneListaEstados();
			data.setListaEstados(listOfRecords);
		} catch (SQLOperationException e) {
			// TODO Auto-generated catch block
			// return a empty row
			listOfRecords = new ArrayList<Estado>();
			data.setListaEstados(listOfRecords);
			LOG.error(e.getMessage(), e);
		}
		if (listOfRecords == null) {
			// return a empty row
			listOfRecords = new ArrayList<Estado>();
			data.setListaEstados(listOfRecords);
			LOG.warn("CONSULTA A CAT_ESTADOS CON RESULTADOS NULOS");
		} else {
			LOG.debug("Records founds [" + listOfRecords.size() + "]");
		}
		return data;
	}
}
