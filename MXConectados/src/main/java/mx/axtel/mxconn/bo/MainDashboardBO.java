package mx.axtel.mxconn.bo;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mx.axtel.mxconn.beans.BandWidthBean;
import mx.axtel.mxconn.beans.DashboardData;
import mx.axtel.mxconn.beans.DashboardRow;
import mx.axtel.mxconn.beans.Estado;
import mx.axtel.mxconn.beans.GidChartsData;
import mx.axtel.mxconn.beans.LatencyBean;
import mx.axtel.mxconn.beans.ProtocolBean;
import mx.axtel.mxconn.beans.TransferRateBean;
import mx.axtel.mxconn.config.StandardConfigurationValues;
import mx.axtel.mxconn.dao.DashboardTableDAO;
import mx.axtel.mxconn.dao.EstadosDAO;
import mx.axtel.mxconn.dao.GraphicsDAO;
import mx.axtel.mxconn.exceptions.SQLOperationException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * The Class MainDashboardBO.
 */
@Service
public class MainDashboardBO {
	
	/** The logger. */
	private static final Logger LOG = Logger.getLogger(MainDashboardBO.class);
	
	private static final String USG_IDENTIFIER = "USG_";
	
	@Autowired
	private DashboardTableDAO dashboardDAO;
	
	@Autowired
	private EstadosDAO estadosDAO;
	
	@Autowired
	private GraphicsDAO graphicsDAO;
	
	/**
	 * Instantiates a new main dashboard bo.
	 */
	public MainDashboardBO() {
		System.out.println("Loading MainDashboardBO");
	}
	
	/**
	 * Build table using GID like Key.
	 *
	 * @param key the key
	 * @return the list
	 */
	public List<DashboardRow> buildMainTableWithGID(String key) {
		List<DashboardRow> listOfRecords = null;
		DashboardRow bean = null;
		try {
			listOfRecords = dashboardDAO.searchByPrimaryKey(key);
		} catch (SQLOperationException e) {
			listOfRecords = new ArrayList<DashboardRow>();
			bean = new DashboardRow();
			listOfRecords.add(bean);
			e.printStackTrace();
		}
		if (listOfRecords == null) {
			// return a single row
			listOfRecords = new ArrayList<DashboardRow>();
			bean = new DashboardRow();
			listOfRecords.add(bean);
			LOG.debug("CONSULTA PRINCIPAL X GID CON RESULTADOS NULOS");
		} else {
			LOG.debug("Records founds [" + listOfRecords.size() + "]");
		}
		return listOfRecords;
	}
	
	/**
	 * Build table using state like Key.
	 *
	 * @param state the state
	 * @return the list
	 * @throws SQLException 
	 */
	public List<DashboardRow> buildMainTableWithStateName(String state) throws SQLException {
		List<DashboardRow> listOfRecords = null;
		DashboardRow bean = null;
		try {
			// TODO Implementar con commons validator
			Integer.parseInt(state);
		} catch(NumberFormatException e) {
			LOG.error("Invalid State Code: " + e.getMessage(), e);
			StandardConfigurationValues conf = StandardConfigurationValues.getInstance();
			state = conf.getDefaultStateKey();
		}
		try {
			listOfRecords = dashboardDAO.searchByIndexColum(state);
		} catch (SQLOperationException e) {
			// TODO Auto-generated catch block
			// return a single row
			listOfRecords = new ArrayList<DashboardRow>();
			bean = new DashboardRow();
			listOfRecords.add(bean);
			e.printStackTrace();
		}
		if (listOfRecords == null) {
			// return a single row
			listOfRecords = new ArrayList<DashboardRow>();
			bean = new DashboardRow();
			listOfRecords.add(bean);
			LOG.debug("CONSULTA PRINCIPAL x ESTADO CON RESULTADOS NULOS");
		} else {
			LOG.debug("Records founds [" + listOfRecords.size() + "]");
		}
		return listOfRecords;
	}
	
	/**
	 * Build table using state like Key.
	 *
	 * @param state the state
	 * @return the list
	 */
	public DashboardData getCatEstados() {
		java.util.List<Estado> listOfRecords = null;
		DashboardData data = new DashboardData();
		try {
			listOfRecords = estadosDAO.obtieneListaEstados();
			data.setListaEstados(listOfRecords);
		} catch (SQLOperationException e) {
			// TODO Auto-generated catch block
			// return a empty row
			listOfRecords = new ArrayList<Estado>();
			data.setListaEstados(listOfRecords);
			LOG.error(e.getMessage(), e);
		}
		if (listOfRecords == null) {
			// return a empty row
			listOfRecords = new ArrayList<Estado>();
			data.setListaEstados(listOfRecords);
			LOG.warn("CONSULTA A CAT_ESTADOS CON RESULTADOS NULOS");
		} else {
			LOG.debug("Records founds [" + listOfRecords.size() + "]");
		}
		return data;
	}
	
	/**
	 * Build graphics info using GID like Key.
	 *
	 * @param state the state
	 * @return the list
	 * @throws SQLException 
	 */
	public GidChartsData getBandWidthInformation(String key) throws SQLException {
		java.util.List<BandWidthBean> listOfBandWidthBeans = null;
		GidChartsData data = new GidChartsData();
		String gid = USG_IDENTIFIER + key;
		try {
			listOfBandWidthBeans = graphicsDAO.getAllBandWidthData(gid);
			data.setAnchoDeBanda(listOfBandWidthBeans);
		} catch (SQLOperationException e) {
			// TODO Auto-generated catch block
			LOG.error(e.getMessage(), e);
		}
		return data;
	}
	
	/**
	 * Build graphics info using GID like Key.
	 *
	 * @param state the state
	 * @return the list
	 * @throws SQLException 
	 */
	public GidChartsData getTranferRateInformation(String key) throws SQLException {
		java.util.List<TransferRateBean> listOfTransferRateBeans = null;
		GidChartsData data = new GidChartsData();
		String gid = USG_IDENTIFIER + key;
		try {
			listOfTransferRateBeans = graphicsDAO.getAllTransferRateData(gid);
			data.setTasaDeTransferencia(listOfTransferRateBeans);
		} catch (SQLOperationException e) {
			// TODO Auto-generated catch block
			LOG.error(e.getMessage(), e);
		}
		return data;
	}
	
	/**
	 * Build graphics info using GID like Key.
	 *
	 * @param state the state
	 * @return the list
	 * @throws SQLException 
	 */
	public GidChartsData getLatencyInformation(String key) throws SQLException {
		java.util.List<LatencyBean> listOfLatencyBeans = null;
		GidChartsData data = new GidChartsData();
		String gid = USG_IDENTIFIER + key;
		try {
			listOfLatencyBeans = graphicsDAO.getAllLatencyData(gid);
			data.setLatencia(listOfLatencyBeans);
		} catch (SQLOperationException e) {
			// TODO Auto-generated catch block
			LOG.error(e.getMessage(), e);
		}
		return data;
	}
	
	/**
	 * Build graphics info using GID like Key.
	 *
	 * @param state the state
	 * @return the list
	 * @throws SQLException 
	 */
	public GidChartsData getProtocolInformation(String key) throws SQLException {
		java.util.List<ProtocolBean> listOfProtocolBeans = null;
		GidChartsData data = new GidChartsData();
		String gid = USG_IDENTIFIER + key;
		try {
			listOfProtocolBeans = graphicsDAO.getAllProtocolData(gid);
			data.setProtocolos(listOfProtocolBeans);
		} catch (SQLOperationException e) {
			// TODO Auto-generated catch block
			LOG.error(e.getMessage(), e);
		}
		return data;
	}
	
	/**
	 * Build all graphics info using GID like Key.
	 *
	 * @param state the state
	 * @return the list
	 * @throws SQLException 
	 */
	public GidChartsData getGraphicsInformation(String key) throws SQLException {
		java.util.List<BandWidthBean> listOfBandWidthBeans = null;
		java.util.List<TransferRateBean> listOfTransferRateBeans = null;
		java.util.List<LatencyBean> listOfLatencyBeans = null;
		java.util.List<ProtocolBean> listOfProtocolBeans = null;
		GidChartsData data = new GidChartsData();
		String gid = USG_IDENTIFIER + key;
		try {
			listOfBandWidthBeans = graphicsDAO.getAllBandWidthData(gid);
			listOfTransferRateBeans = graphicsDAO.getAllTransferRateData(gid);
			listOfLatencyBeans = graphicsDAO.getAllLatencyData(gid);
			listOfProtocolBeans = graphicsDAO.getAllProtocolData(gid);
			data.setAnchoDeBanda(listOfBandWidthBeans);
			data.setTasaDeTransferencia(listOfTransferRateBeans);
			data.setLatencia(listOfLatencyBeans);
			data.setProtocolos(listOfProtocolBeans);
		} catch (SQLOperationException e) {
			// TODO Auto-generated catch block
			LOG.error(e.getMessage(), e);
		}
		return data;
	}
}
