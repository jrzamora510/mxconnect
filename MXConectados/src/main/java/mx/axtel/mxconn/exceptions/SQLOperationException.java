package mx.axtel.mxconn.exceptions;

/**
 * The Class SQLOperationException.
 */
public class SQLOperationException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new SQL operation exception.
	 */
	public SQLOperationException() {
	}

	/**
	 * Instantiates a new SQL operation exception.
	 *
	 * @param arg0 the arg0
	 */
	public SQLOperationException(String arg0) {
		super(arg0);
	}

	/**
	 * Instantiates a new SQL operation exception.
	 *
	 * @param arg0 the arg0
	 */
	public SQLOperationException(Throwable arg0) {
		super(arg0);
	}

	/**
	 * Instantiates a new SQL operation exception.
	 *
	 * @param arg0 the arg0
	 * @param arg1 the arg1
	 */
	public SQLOperationException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	/**
	 * Instantiates a new SQL operation exception.
	 *
	 * @param arg0 the arg0
	 * @param arg1 the arg1
	 * @param arg2 the arg2
	 * @param arg3 the arg3
	 */
	public SQLOperationException(String arg0, Throwable arg1, boolean arg2,
			boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}
}
