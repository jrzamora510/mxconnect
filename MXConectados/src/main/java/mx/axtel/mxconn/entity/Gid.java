package mx.axtel.mxconn.entity;

public class Gid {
	
	private String gid;
	
	private String institucion;
	private String nombreDelEstado;
	private String OrderModem;
	private String OrdenIpFija;
	private String ipFija;
	private String estatus;
	private String fecha;
	private String ipFijaEneSight;
	private String validacionIp;
	private String notas;
	private String sitioGid;
	private String folio;
	private String receptor;
	private String fechaFirmaActaInicioOper;
	private String NoSerieUSG;
	private String macAddres;
	private String direccionIPHomologada;
	private String comunidadSNMP;
	private String ipOficioSCT;
	private String altaCIMOV;
	private String fechanotificadoCSIC;
	private String autorizadoCSICMenor20Dias;
	private String ProveedorMt;
	private String fechaPruebas;
	private String suspendidoSCTOficio;
	private String blankColumn = "-";
	/**
	 * @return the gid
	 */
	public String getGid() {
		return gid;
	}
	/**
	 * @param gid the gid to set
	 */
	public void setGid(String gid) {
		this.gid = gid;
	}
	/**
	 * @return the istitucion
	 */
	public String getInstitucion() {
		return institucion;
	}
	/**
	 * @param istitucion the istitucion to set
	 */
	public void setInstitucion(String institucion) {
		this.institucion = institucion;
	}
	/**
	 * @return the nombreDelEstado
	 */
	public String getNombreDelEstado() {
		return nombreDelEstado;
	}
	/**
	 * @param nombreDelEstado the nombreDelEstado to set
	 */
	public void setNombreDelEstado(String nombreDelEstado) {
		this.nombreDelEstado = nombreDelEstado;
	}
	/**
	 * @return the orderModem
	 */
	public String getOrderModem() {
		return OrderModem;
	}
	/**
	 * @param orderModem the orderModem to set
	 */
	public void setOrderModem(String orderModem) {
		OrderModem = orderModem;
	}
	/**
	 * @return the ordenIpFija
	 */
	public String getOrdenIpFija() {
		return OrdenIpFija;
	}
	/**
	 * @param ordenIpFija the ordenIpFija to set
	 */
	public void setOrdenIpFija(String ordenIpFija) {
		OrdenIpFija = ordenIpFija;
	}
	/**
	 * @return the ipFija
	 */
	public String getIpFija() {
		return ipFija;
	}
	/**
	 * @param ipFija the ipFija to set
	 */
	public void setIpFija(String ipFija) {
		this.ipFija = ipFija;
	}
	/**
	 * @return the estatus
	 */
	public String getEstatus() {
		return estatus;
	}
	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	/**
	 * @return the fecha
	 */
	public String getFecha() {
		return fecha;
	}
	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	/**
	 * @return the ipFijaEneSight
	 */
	public String getIpFijaEneSight() {
		return ipFijaEneSight;
	}
	/**
	 * @param ipFijaEneSight the ipFijaEneSight to set
	 */
	public void setIpFijaEneSight(String ipFijaEneSight) {
		this.ipFijaEneSight = ipFijaEneSight;
	}
	/**
	 * @return the validacionIp
	 */
	public String getValidacionIp() {
		return validacionIp;
	}
	/**
	 * @param validacionIp the validacionIp to set
	 */
	public void setValidacionIp(String validacionIp) {
		this.validacionIp = validacionIp;
	}
	/**
	 * @return the notas
	 */
	public String getNotas() {
		return notas;
	}
	/**
	 * @param notas the notas to set
	 */
	public void setNotas(String notas) {
		this.notas = notas;
	}
	/**
	 * @return the sitioGid
	 */
	public String getSitioGid() {
		return sitioGid;
	}
	/**
	 * @param sitioGid the sitioGid to set
	 */
	public void setSitioGid(String sitioGid) {
		this.sitioGid = sitioGid;
	}
	/**
	 * @return the folio
	 */
	public String getFolio() {
		return folio;
	}
	/**
	 * @param folio the folio to set
	 */
	public void setFolio(String folio) {
		this.folio = folio;
	}
	/**
	 * @return the receptor
	 */
	public String getReceptor() {
		return receptor;
	}
	/**
	 * @param receptor the receptor to set
	 */
	public void setReceptor(String receptor) {
		this.receptor = receptor;
	}
	/**
	 * @return the fechaFirmaActaInicioOper
	 */
	public String getFechaFirmaActaInicioOper() {
		return fechaFirmaActaInicioOper;
	}
	/**
	 * @param fechaFirmaActaInicioOper the fechaFirmaActaInicioOper to set
	 */
	public void setFechaFirmaActaInicioOper(String fechaFirmaActaInicioOper) {
		this.fechaFirmaActaInicioOper = fechaFirmaActaInicioOper;
	}
	/**
	 * @return the noSerieUSG
	 */
	public String getNoSerieUSG() {
		return NoSerieUSG;
	}
	/**
	 * @param noSerieUSG the noSerieUSG to set
	 */
	public void setNoSerieUSG(String noSerieUSG) {
		NoSerieUSG = noSerieUSG;
	}
	/**
	 * @return the macAddres
	 */
	public String getMacAddres() {
		return macAddres;
	}
	/**
	 * @param macAddres the macAddres to set
	 */
	public void setMacAddres(String macAddres) {
		this.macAddres = macAddres;
	}
	/**
	 * @return the direccionIPHomologada
	 */
	public String getDireccionIPHomologada() {
		return direccionIPHomologada;
	}
	/**
	 * @param direccionIPHomologada the direccionIPHomologada to set
	 */
	public void setDireccionIPHomologada(String direccionIPHomologada) {
		this.direccionIPHomologada = direccionIPHomologada;
	}
	/**
	 * @return the comunidadSNMP
	 */
	public String getComunidadSNMP() {
		return comunidadSNMP;
	}
	/**
	 * @param comunidadSNMP the comunidadSNMP to set
	 */
	public void setComunidadSNMP(String comunidadSNMP) {
		this.comunidadSNMP = comunidadSNMP;
	}
	/**
	 * @return the ipOficioSCT
	 */
	public String getIpOficioSCT() {
		return ipOficioSCT;
	}
	/**
	 * @param ipOficioSCT the ipOficioSCT to set
	 */
	public void setIpOficioSCT(String ipOficioSCT) {
		this.ipOficioSCT = ipOficioSCT;
	}
	/**
	 * @return the altaCIMOV
	 */
	public String getAltaCIMOV() {
		return altaCIMOV;
	}
	/**
	 * @param altaCIMOV the altaCIMOV to set
	 */
	public void setAltaCIMOV(String altaCIMOV) {
		this.altaCIMOV = altaCIMOV;
	}
	/**
	 * @return the fechanotificadoCSIC
	 */
	public String getFechanotificadoCSIC() {
		return fechanotificadoCSIC;
	}
	/**
	 * @param fechanotificadoCSIC the fechanotificadoCSIC to set
	 */
	public void setFechanotificadoCSIC(String fechanotificadoCSIC) {
		this.fechanotificadoCSIC = fechanotificadoCSIC;
	}
	/**
	 * @return the autorizadoCSICMenor20Dias
	 */
	public String getAutorizadoCSICMenor20Dias() {
		return autorizadoCSICMenor20Dias;
	}
	/**
	 * @param autorizadoCSICMenor20Dias the autorizadoCSICMenor20Dias to set
	 */
	public void setAutorizadoCSICMenor20Dias(String autorizadoCSICMenor20Dias) {
		this.autorizadoCSICMenor20Dias = autorizadoCSICMenor20Dias;
	}
	/**
	 * @return the proveedorMt
	 */
	public String getProveedorMt() {
		return ProveedorMt;
	}
	/**
	 * @param proveedorMt the proveedorMt to set
	 */
	public void setProveedorMt(String proveedorMt) {
		ProveedorMt = proveedorMt;
	}
	/**
	 * @return the fechaPruebas
	 */
	public String getFechaPruebas() {
		return fechaPruebas;
	}
	/**
	 * @param fechaPruebas the fechaPruebas to set
	 */
	public void setFechaPruebas(String fechaPruebas) {
		this.fechaPruebas = fechaPruebas;
	}
	/**
	 * @return the suspendidoSCTOficio
	 */
	public String getSuspendidoSCTOficio() {
		return suspendidoSCTOficio;
	}
	/**
	 * @param suspendidoSCTOficio the suspendidoSCTOficio to set
	 */
	public void setSuspendidoSCTOficio(String suspendidoSCTOficio) {
		this.suspendidoSCTOficio = suspendidoSCTOficio;
	}
	/**
	 * @return the blankColumn
	 */
	public String getBlankColumn() {
		return blankColumn;
	}
	/**
	 * @param blankColumn the blankColumn to set
	 */
	public void setBlankColumn(String blankColumn) {
		this.blankColumn = blankColumn;
	}

}
